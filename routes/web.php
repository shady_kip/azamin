<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','client\HomeController@home');
Route::get('/auth/login','Auth\LoginController@index')->name('login');
Route::get('/create-account','Auth\RegisterController@index');

Route::get('/products','client\ProductsController@getProducts');

Route::get('/product/{slug}','client\HomeController@getproduct');
Route::get('/products/category/{slug}','client\HomeController@filter_by_categories');

Route::post('/new/create','Auth\RegisterController@create');
Route::post('/user/login','Auth\LoginController@login');
Route::post('/logout','Auth\LoginController@logout');


Route::get('/dashboard','dashboard\DashboardController@index')->middleware('auth');
Route::get('/dashboard/physical/category','dashboard\DashboardController@physical_category')->middleware('auth');
Route::get('/dashboard/physical/category/sub/{sub_id}','dashboard\DashboardController@get_sub_category')->middleware('auth');
Route::post('/physical/category/create','dashboard\CategoryController@store')->middleware('auth');
Route::post('/physical/category/edit/{id}','dashboard\CategoryController@update')->middleware('auth');
Route::get('/physical/category/delete/{id}','dashboard\CategoryController@destroy')->middleware('auth');
Route::get('/dashboard/physical/sub-category','dashboard\DashboardController@physical_sub_category')->middleware('auth');
Route::post('/dashboard/physical/sub-category','dashboard\CategoryController@store_sub')->middleware('auth');

Route::get('/dashboard/physical/product-lists','dashboard\DashboardController@product_lists')->middleware('auth');

Route::get('/dashboard/physical/create-product','dashboard\DashboardController@create_product')->middleware('auth');
Route::post('/dashboard/physical/create-product','dashboard\DashboardController@store_product')->middleware('auth');
Route::post('/dashboard/physical/product/update/{id}','dashboard\DashboardController@update_product')->middleware('auth');
Route::get('/dashboard/physical/edit/{id}','dashboard\ProductController@show')->middleware('auth');
Route::get('/dashboard/physical/product/{slug}','dashboard\ProductController@details')->middleware('auth');
Route::get('/product/destroy/{id}','dashboard\ProductController@destroy')->middleware('auth');
Route::post('/product/variation/create/{id}','dashboard\VariationController@store')->middleware('auth');
Route::get('/dashboard/physical/product/variation/add/{id}','dashboard\VariationController@add')->middleware('auth');
Route::post('/dashboard/physical/product/variation/create/{id}','dashboard\VariationController@insert')->middleware('auth');

Route::post('/physical/category/variation/create','dashboard\VariationController@create')->middleware('auth');
Route::get('/product/variation/destroy/{id}','dashboard\VariationController@destroy')->middleware('auth');





Route::post('/dashboard/physical/upload-image','dashboard\DashboardController@uploadImage')->middleware('auth');
Route::get('/dashboard/digital/create-product','dashboard\DashboardController@create_product')->middleware('auth');
Route::get('/dashboard/sub-category','dashboard\DashboardController@sub-category')->middleware('auth');
Route::get('/dashboard/all-products','dashboard\DashboardController@sub-category')->middleware('auth');
Route::get('/dashboard/create-product','dashboard\DashboardController@create-product')->middleware('auth');

Route::get('/category/{category}','client\CategoryController@loadProducts');


Route::get('/getSubcategory/{id}','dashboard\DashboardController@getSubCategory')->middleware('auth');
Route::post('/images-save/{id}', 'dashboard\DashboardController@store_product_images');
Route::post('/images-delete', 'UploadImagesController@destroy');
Route::get('/images-show', 'UploadImagesController@index');


Route::get('/dashboard/sales/orders','Dashboard\OrdersController@index')->middleware('auth');
Route::get('/dashboard/sales/transactions','Dashboard\TransactionsController@index')->middleware('auth');


Route::get('/dashboard/coupons','Dashboard\DashboardController@index')->middleware('auth');
Route::get('/dashboard/create','Dashboard\DashboardController@show')->middleware('auth');

Route::get('/dashboard/users','Dashboard\UsersController@index')->middleware('auth');
Route::get('/dashboard/new','Dashboard\UsersController@create')->middleware('auth');

Route::get('/dashboard/settings','Dashboard\SettingsController@index')->middleware('auth');
Route::get('/dashboard/physical/attributes','Dashboard\SettingsController@attributes')->middleware('auth');
Route::post('/dashboard/physical/product/attributes/create','dashboard\SettingsController@createAttribute')->middleware('auth');


Route::resource('/dashboard/brands','BrandController');

