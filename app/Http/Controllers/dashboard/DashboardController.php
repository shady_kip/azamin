<?php

namespace App\Http\Controllers\dashboard;

use App\Category;
use App\Gallery;
use App\Http\Controllers\Controller;
use App\Product;
use App\Sub_category;
use App\Variation;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class DashboardController extends Controller
{
    public $products_image_path;
    private $original_photos_path;
    private $resized_photos_path;
    public $prod_id;
    public function __construct()
    {
        $this->products_image_path=public_path('/product_images');
        $this->resized_photos_path = public_path('/product_images/resized');
        $this->original_photos_path = public_path('/product_images/original');


    }
    public function index(){
        return view('dashboard.dashboard');
    }
    public function physical_category(){
        $categories = Category::with('ancestors')->get()->toTree();

        return view('dashboard.products.physical.list-categories')->with('categories',$categories);
    }
    public function get_sub_category(Request $request){
       // $categories = Category::with('ancestors')->get()->toTree();
        $categories=Category::whereDescendantOf($request->sub_id)->get()->toTree();

        return view('dashboard.products.physical.subcat')->with('categories',$categories);
    }

    public function physical_sub_category(){
        $sub_categories= Sub_category::with('categories')->orderBy('id','DESC')->get();
        return view('dashboard.products.physical.list-sub-categories')->with('subcategories',$sub_categories);
    }
    public function getSubCategory($id){
        $sub_categories= Category::descendantsOf($id);
        foreach ($sub_categories as $sub){
            if($sub->id!==$id) {
                echo "<option value='$sub->id'>$sub->name</option>";
            }
        }


    }
    public function product_lists(){
        $products = Product::latest()->get();
        $categories = Category::with('ancestors')->get()->toTree();

        return view('dashboard.products.physical.list-physical-products')->with(['products'=>$products,'categories'=>$categories]);
    }
    public function create_product(){
        $categories = Category::with('ancestors')->get()->toTree();
        return view('dashboard.products.forms.create-product')->with(['categories'=>$categories]);
    }

    public function store_product(Request $request){
        $product = new Product();
        $product->title=$request->title;
        $product->description=$request->description;
        $product->features=$request->features;
        $product->SKU=$request->product_sku;
        if($request->featured==1) {
            $product->featured = 1;
        };
        $product->cost_price=$request->cost_price;
        $product->selling_price=$request->selling_price;
        $product->stock_id=$request->stock_id;
        $product->brand_id=$request->brand_id;
        $product->category_id=$request->category_id;
        $product->product_type=$request->productType;
        if($product->save()){
            $this->prod_id=$product->id;

            foreach (json_decode($request->product_variations) as $varia){
                $variation=new Variation();
                $variation->name = $varia->name;
                $variation->variation_value = $varia->var_value;
                $variation->price = $varia->var_price;
                $variation->product_id=$product->id;
                $variation->save();
            }

           // $product->categories()->sync($request->category);
           return response()->json([
               "message"=>true,
               "product"=>$product
           ]);
        }else{
            echo false;
        }
    }


    public function update_product(Request $request,$id){
        $product = Product::find($id);
            $product->title=$request->title;
        $product->description=$request->description;
        $product->features=$request->features;
        $product->SKU=$request->product_sku;
        if($request->featured==1) {
            $product->featured = 1;
        };
        $product->cost_price=$request->cost_price;
        $product->selling_price=$request->selling_price;
        $product->stock_id=$request->stock_id;
        $product->brand_id=$request->brand;
        $product->category_id=$request->sub_category;
        $product->save();
        return response()->json([
            "message"=>"saved",
            "product"=>$product
        ]);
    }

    public function uploadImage(Request $request){
        if($request->hasFile('featured_image')){
            if (!is_dir($this->products_image_path)) {
                mkdir($this->products_image_path, 0777,true);
            }
            $oname=basename($request->file('featured_image')->getClientOriginalName());
            $new_name = $request->id.'_'.$oname;
            $original_img=Image::make($request->file('featured_image'));
            $resized_img=Image::make($request->file('featured_image'))
                ->fit(333, 250, function ($constraints) {
                    //  $constraints->aspectRatio();
                    $constraints->upsize();
                });
            $resized_img->save($this->products_image_path . '/resized/' . $new_name);
            $original_img->save($this->products_image_path . '/original/' . $new_name);
            $product=Product::find($request->id);
            $product->featured_image=$new_name;
            $product->save();
        }
    }

    public function store_product_images(Request $request,$id){
        $photos = $request->file('file');

        if (!is_array($photos)) {
            $photos = [$photos];
        }
        for ($i = 0; $i < count($photos); $i++) {
            $photo = $photos[$i];
            $oname = basename($photo->getClientOriginalName());
            $name = time() . '_' . $oname;
            $save_name = $name;
            $resize_name = $name;

            $img = Image::make($photo)
                ->fit(333, 250, function ($constraints) {
                    //  $constraints->aspectRatio();
                    $constraints->upsize();
                });
            $img2 = Image::make($photo);
            $img->save($this->resized_photos_path . '/' . $resize_name);
            $img2->save($this->original_photos_path . '/' . $save_name);
            $upload = new Gallery();
            $upload->name = $save_name;
            $upload->product_id = $id;
            $upload->save();
        }
    }
}
