<?php

namespace App\Http\Controllers\dashboard;

use App\Attribute;
use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\Variation;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class VariationController extends Controller
{
    private $variations_image_path;
    public function __construct()
    {
        $this->variations_image_path=public_path('/variation_images');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'parent' => 'required',
        ]);

        $var_values=explode(',',$request->var_values);
        $_var=json_decode($var_values);


            Variation::create([
            'name'=>$request->attribute,
            'category_id'=>$request->parent,
            'variation_value'=>$_var
        ]);




        //
    }

    public function add($id){
        return view('dashboard.products.forms.variant')->with(['product_id'=>$id]);
    }

    public function insert(Request $request,$id){
        $variation = new Variation();
        $variation->name=ucwords($request->att_name);
        $variation->variation_value=ucwords($request->att_value);
        $variation->price=$request->att_price;
        $variation->var_symbol=strtoupper($request->att_symbol);
        $variation->product_id=$id;

        $variation->save();
        return redirect()->back();
    }

    public function attributes(Request $request,$id){

         $attributes = new Attribute();
         $attributes->name=ucfirst($request->att_name);
         $attributes->save();
         return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $variation = new Variation();
        $variation->name = $request->name;
        $variation->variation_value = $request->var_value;
        $variation->price = $request->var_price;
        $variation->product_id=$id;


        if($request->hasFile('var_image')){
            if (!is_dir($this->variations_image_path)) {
                mkdir($this->variations_image_path, 0777,true);
            }
            $oname=basename($request->file('var_image')->getClientOriginalName());
            $new_name = $request->id.'_'.$oname;
            $original_img=Image::make($request->file('var_image'));
            $resized_img=Image::make($request->file('var_image'))
                ->fit(333, 250, function ($constraints) {
                    //  $constraints->aspectRatio();
                    $constraints->upsize();
                });
            $resized_img->save($this->variations_image_path . '/resized/' . $new_name);
            $original_img->save($this->variations_image_path . '/original/' . $new_name);
            $variation->var_image = $new_name;
            $variation->save();
            return response()->json([
                'message'=>true,
                'variation'=>$variation
            ]);
        }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $variation = Variation::find($id);
        $variation->delete();
        return response()->json([
            "message"=>true
        ]);
    }
}
