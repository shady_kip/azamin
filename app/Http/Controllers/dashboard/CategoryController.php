<?php

namespace App\Http\Controllers\dashboard;

use App\Category;
use App\categories_subcategory;
use App\Http\Controllers\Controller;
use App\Sub_category;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class CategoryController extends Controller
{
    public $categories_image_path;
    public function __construct()
    {
        $this->categories_image_path=public_path('/category_images');


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function store_sub(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'sub_name' => 'required',
        ]);
        $sub=new Sub_category();
        $sub->category_id=$request->category_id;
        $sub->name=$request->sub_name;
        $sub->save();
       $category_id=$request->category_id;
       $sub->categories()->sync($category_id);
        return redirect('/dashboard/physical/sub-category');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'category' => 'required',
        ]);
        //


        if($request->hasFile('cat_image')){
            if (!is_dir($this->categories_image_path)) {
                mkdir($this->categories_image_path, 0777);
            }
            $oname=basename($request->file('cat_image')->getClientOriginalName());
            $new_name = time().'_'.$oname;
            $img=Image::make($request->file('cat_image'))
                ->fit(333, 250, function ($constraints) {
                    //  $constraints->aspectRatio();
                    $constraints->upsize();
                });
            $img->save($this->categories_image_path . '/' . $new_name);



        }else{
            $new_name='noimage.jpg';
        }




        $category = Category::create([
            'name'=>$request->category,
            'image'=>$new_name
        ]);



        if($request->parent && $request->parent !== 'none') {
            //  Here we define the parent for new created category
            $node = Category::find($request->parent);

            $node->appendNode($category);
        }

//        if($request->added_from_product_page=='1'){
//            if($request->sub_name!=='') {
//                $subcategoy = new Sub_category();
//                $subcategoy->category_id = $category->id;
//                $subcategoy->name = $request->sub_name;
//                $subcategoy->save();
//                $subcategoy->categories()->sync($category->id);
//            }
//
//
//            $categories = Category::orderBy('created_at','DESC')->get();
//            $sel = "";
//            foreach ($categories as $cat){
//                echo "<option value='$cat->id'>$cat->name</option>";
//            }
//
//        }else{
//            return redirect('/dashboard/physical/category');
//        }
        return redirect('/dashboard/physical/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $category= Category::find($id);
        $category->name=$request->cat_name;
        if($request->hasFile('cat_image')){
            if (!is_dir($this->categories_image_path)) {
                mkdir($this->categories_image_path, 0777);
            }
            $oname=basename($request->file('cat_image')->getClientOriginalName());
            $new_name = time().'_'.$oname;
            $img=Image::make($request->file('cat_image'))
                ->fit(333, 250, function ($constraints) {
                    //  $constraints->aspectRatio();
                    $constraints->upsize();
                });
            $img->save($this->categories_image_path . '/' . $new_name);


            $category->image=$new_name;
        }


        $category->save();
        return redirect('/dashboard/physical/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        if($category->delete()){
            $sub_cat = Sub_category::where("category_id",$id);
            $sub_cat->delete();
            $category->sub_categories()->wherePivot('category_id','=',$id)->detach();

            return response()->json([
                "message"=>true
            ]);
        }

        //
    }
}
