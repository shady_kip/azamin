<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;
use Intervention\Image\Image;

class UploadImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $original_photos_path;
    private $resized_photos_path;

    public function __construct()
    {

        $this->resized_photos_path = public_path('/product_images/resized');
        $this->original_photos_path = public_path('/product_images/original');

    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $photos = $request->file('file');

        if (!is_array($photos)) {
            $photos = [$photos];
        }


        for ($i = 0; $i < count($photos); $i++) {
            $photo = $photos[$i];
            $oname=basename($photo->getClientOriginalName());
            $name = time().'_'.$oname;
            $save_name = $name;
            $resize_name = $name;

            $img=Image::make($photo)
                ->fit(333, 250, function ($constraints) {
                    //  $constraints->aspectRatio();
                    $constraints->upsize();
                });
            $img2=Image::make($photo);
            $img->save($this->resized_photos_path . '/' . $resize_name);
            $img2->save($this->photos_path . '/' . $save_name);
            $upload = new Gallery();
            $upload->name = $save_name;
            $upload->product_id=$request->id;
            $upload->save();
        }

        return response()->json([
            'message' => true
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
