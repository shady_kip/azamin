<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    //
    public function getProducts(Request $request){
        $products = Product::latest()->get();

        return view('client.products')->with('products',$products);

    }
}
