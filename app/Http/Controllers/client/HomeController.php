<?php

namespace App\Http\Controllers\client;

use App\Brand;
use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function home(){

        return view('client.home');

    }

    public function getproduct(Request $request){
        $slug=$request->slug;
        $product=Product::where('slug',$slug)->first();

        return view('client.single-product')->with('product',$product);
    }

    public function filter_by_categories(Request $request)
    {
        $category2 = Category::where('slug', $request->slug)->first();
        $category = Category::descendantsAndSelf($category2->id);
        $categories = $category->pluck('id');
        $product = Product::whereIn('category_id', $categories)->get();
        $getBrands = $product->pluck('brand_id');
        $brands = Brand::whereIn('id', $getBrands)->get();

        if (isset($_SERVER['QUERY_STRING'])) {

        $query = explode('&', $_SERVER['QUERY_STRING']);
        $params = array();

        foreach ($query as $param) {
            list($name, $value) = explode('=', $param);
            $params[urldecode($name)][] = urldecode($value);
        }
        $getBrandsIds = array();

        foreach ($params as $par) {
            foreach ($par as $brand2) {

                $bra = Brand::where('name', $brand2)->get();
                foreach ($bra as $br) {
                    array_push($getBrandsIds, $br->id);
                }
            }
        }
        if (count($getBrandsIds) > 0) {
            $product = $product->whereIn('brand_id', $getBrandsIds);
        }
    }
       return view('client.products')->with(['products'=>$product,'sub_cat'=>$category2,'brands'=>$brands]);
    }
}
