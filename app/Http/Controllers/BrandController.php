<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::latest()->get();
        return view('dashboard.products.physical.brands')->with('brands',$brands);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('dashboard.products.forms.create-brand');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if($request->hasFile('logo_image')){
            if (!is_dir(public_path('/brands'))) {
                mkdir(public_path('/brands'), 0777,true);
            }
            $oname=basename($request->file('logo_image')->getClientOriginalName());
            $new_name = time().'_'.$oname;
            $original_img=Image::make($request->file('logo_image'));
            $resized_img=Image::make($request->file('logo_image'))
                ->fit(333, 250, function ($constraints) {
                    //  $constraints->aspectRatio();
                    $constraints->upsize();
                });
            $resized_img->save(public_path('/brands') . '/resized/' . $new_name);
            $original_img->save(public_path('/brands') . '/original/' . $new_name);

        }else{
            $new_name='no-lmage.jpg';
        }

        $brands=Brand::create([
            'name'=>$request->brand,
            'logo'=>$new_name
        ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        //
    }
}
