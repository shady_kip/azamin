<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Product extends Model
{
    //
    use HasSlug;


    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom(['title','stock_id'])
            ->saveSlugsTo('slug');
    }

    public function categories(){
    return $this->belongsToMany(Category::class);
    }
    public function variations(){
        return $this->belongsToMany(Variation::class);
    }
}
