<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Variation extends Model
{
    //

    protected $fillable = [
        'name', 'variation_value', 'var_image','category_id'
    ];

    public function products(){
        return $this->belongsToMany(Product::class);
    }
}
