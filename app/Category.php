<?php

namespace App;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    use HasSlug;
    use NodeTrait;

    protected $fillable = [
        'name', 'slug', 'image',
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
    public function sub_categories()
    {
        return $this->belongsToMany(Sub_category::class);
    }
    public function products(){
        return $this->belongsToMany(Product::class);
    }
}
