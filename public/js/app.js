/**
 * Created by _ck_ on 3/6/2020.
 */
let isOpen=false;
$('.hidden-icon').hide();



$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();

    if(scroll>200){

        $('.nav-primary').addClass('desktop-sticky')
        $('.desktop-sticky').fadeIn({
            duration:1000
        })

        $('.mobile-search-form').addClass('mob-sticky')
    }else {
        $('.nav-primary').removeClass('desktop-sticky');
        $('.mobile-search-form').removeClass('mob-sticky')
    }
});
function showLeftSideBar() {
    if(isOpen){
        document.querySelector("#sidenav").style.width = "0px";
        document.querySelector(".all-content").style.marginLeft = "0px"
        $('.menu-icon').show();
        $('.hidden-icon').hide();
        isOpen=false;
    }else{
        document.querySelector("#sidenav").style.width = "250px";
        document.querySelector(".all-content").style.marginLeft = "250px";
        $('.menu-icon').hide();
        $('.hidden-icon').show();
        isOpen=true;
    }
    
}

function closeNav() {
    document.querySelector("#sidenav").style.width = "0px";
    document.querySelector(".all-content").style.marginLeft = "0px";
    $('.menu-icon').show();
    $('.hidden-icon').hide();
    isOpen=false;
}
$(".hs-menubar").hsMenu();

