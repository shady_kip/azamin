/**
 * Created by _ck_ on 3/6/2020.
 */
$(document).ready(function() {
    $('.owl-carousel1').owlCarousel({
        responsiveClass: true,
        navSpeed: 1000,
        rewind:true,
        autoplay: true,
        dots:true,
        merge:true,
        nav:false,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            }
        },
        loop: true
    });

    $('.owl-carousel2').owlCarousel(
        {
            loop: true,
            responsiveClass: true,
            navSpeed: 1000,
            margin:3,
            rewind:true,
            autoplay: true,
            navText: ['prev', 'next'],
            nav:true,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                375: {
                    items: 2,
                    nav: false
                },
                1000: {
                    items: 5,
                    nav: true,
                    loop: false
                }
            }
        }
    )

    navNext = (category) =>{
        window.location.href='/products/category/'+category
    }



});