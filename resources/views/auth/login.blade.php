@extends('adminlte::master')

@section('adminlte_css_pre')
    <link rel="stylesheet" href="{{ asset('vendor/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{asset('/css/custom.css')}}">
@stop

@section('adminlte_css')
    @stack('css')
    @yield('css')
@stop

@section('classes_body', 'login-page-custom')
@section('body')
<div class="page-overlay">
    <div class="container">
        <div class="row">

            <div class="col-lg-2">

            </div>
            <div class="col-lg-8">
                <div class="login-parent">
                    <div class="login-left">
                        <br><br><br><br>
                       <div class="login-slider  text-center">
                           <div>
                               <h4 class="text-white text-bold">Welcome To Azamin</h4>
                               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet blanditiis delectus provident. Alias atque aut dicta </p>

                           </div>
                           <div>
                               <h4 class="text-white text-bold">Welcome To Azamin</h4>
                               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet blanditiis delectus provident. Alias atque aut dicta </p>
                           </div>
                           <div>
                               <h4 class="text-white text-bold">Welcome To Azamin</h4>
                               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet blanditiis delectus provident. Alias atque aut dicta </p>

                           </div>
                           <div>
                               <h4 class="text-white text-bold">Welcome To Azamin</h4>
                               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet blanditiis delectus provident. Alias atque aut dicta </p>

                           </div>
                       </div>
                    </div>
                    <div class="login-rignt">
                       <div class="login-right-content">
                           <br>

                           <div  class="card login-card">
                               <div  class="card-body  login-card-body">
                                 <h4>Login</h4>
                                   <form action="/user/login" method="post">
                                       {{ csrf_field() }}
                                       <div class="input-group mb-3">
                                           <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{ __('adminlte::adminlte.email') }}" autofocus>
                                           <div class="input-group-append">
                                               <div class="input-group-text">
                                                   <span class="fas fa-envelope"></span>
                                               </div>
                                           </div>
                                           @if ($errors->has('email'))
                                               <div class="invalid-feedback">
                                                   {{ $errors->first('email') }}
                                               </div>
                                           @endif
                                       </div>
                                       <div class="input-group mb-3">
                                           <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="{{ __('adminlte::adminlte.password') }}">
                                           <div class="input-group-append">
                                               <div class="input-group-text">
                                                   <span class="fas fa-lock"></span>
                                               </div>
                                           </div>
                                           @if ($errors->has('password'))
                                               <div class="invalid-feedback">
                                                   {{ $errors->first('password') }}
                                               </div>
                                           @endif
                                       </div>
                                       <div class="row">
                                           <div class="col-12">
                                               <div class="icheck-primary">
                                                   <input type="checkbox" name="remember" id="remember">
                                                   <label for="remember">{{ __('adminlte::adminlte.remember_me') }}</label>
                                               </div>
                                           </div>
                                           <div class="col-lg-12">
                                               <button type="submit" class="btn btn-warning btn-block btn-flat">
                                                   {{ __('adminlte::adminlte.sign_in') }}
                                               </button>
                                           </div>
                                       </div>
                                   </form>
                                   <div class="text-center mt-2">
                                       <b>OR</b>
                                   </div>
                                   <a style="background-color: ghostwhite !important;cursor:pointer;border:none !important;" class="mt-3 text-black-50 btn btn-primary btn-flat btn-block">
                                       Login With Google
                                       <img style="max-height: 25px" class="float-right" src="{{asset('/images/g-logo.png')}}">
                                   </a>
                                   <div class="row">
                                       <div class="col-lg-6">
                                           <p class="mt-2 mb-1">
                                               <a href="">
                                                   {{ __('forgot password?') }}
                                               </a>
                                           </p>
                                       </div>

                                       <div class="col-lg-6">
                                           <p class="mb-1 mt-2">
                                               <a class="float-right" style="color: black" href="/create-account">
                                                   <i class="fa fa-lock"></i> &nbsp;{{ __('Sign Up') }}
                                               </a>
                                           </p>
                                       </div>
                                   </div>
                               </div>



                               </div>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">

            </div>
        </div>
{{--    <div  class="login-box">--}}

{{--      --}}

{{--    </div>--}}
    </div>
</div>
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
