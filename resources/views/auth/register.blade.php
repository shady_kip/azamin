@extends('adminlte::master')

@section('adminlte_css')
    @stack('css')
    @yield('css')
@stop

@section('classes_body', 'register-custom-page')

@php( $login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login') )
@php( $register_url = View::getSection('register_url') ?? config('adminlte.register_url', 'register') )
@php( $dashboard_url = View::getSection('dashboard_url') ?? config('adminlte.dashboard_url', 'home') )

@if (config('adminlte.use_route_url', false))
    @php( $login_url = $login_url ? route($login_url) : '' )
    @php( $register_url = $register_url ? route($register_url) : '' )
    @php( $dashboard_url = $dashboard_url ? route($dashboard_url) : '' )
@else
    @php( $login_url = $login_url ? url($login_url) : '' )
    @php( $register_url = $register_url ? url($register_url) : '' )
    @php( $dashboard_url = $dashboard_url ? url($dashboard_url) : '' )
@endif

@section('body')
    <div class="register-page-overlay">
        <div class="container">
            <br/>
            <br/>
            <br/>
            <br/>
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4">
                    <div style="width: 100% !important;" class="register-box">

                        <div class="card">
                            <div class="card-body register-card-body">
                                <p class="login-box-msg">{{ __('Create an account') }}</p>
                                <form action="/new/create" method="post">
                                    {{ csrf_field() }}

                                    <div class="input-group mb-3">
                                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ old('name') }}"
                                               placeholder="{{ __('adminlte::adminlte.full_name') }}" autofocus>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-user"></span>
                                            </div>
                                        </div>

                                        @if ($errors->has('name'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ old('email') }}"
                                               placeholder="{{ __('adminlte::adminlte.email') }}">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-envelope"></span>
                                            </div>
                                        </div>
                                        @if ($errors->has('email'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                                               placeholder="{{ __('adminlte::adminlte.password') }}">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-lock"></span>
                                            </div>
                                        </div>
                                        @if ($errors->has('password'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="password" name="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"
                                               placeholder="{{ __('adminlte::adminlte.retype_password') }}">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-lock"></span>
                                            </div>
                                        </div>
                                        @if ($errors->has('password_confirmation'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                    <button type="submit" class="btn btn-warning btn-block btn-flat">
                                        {{ __('adminlte::adminlte.register') }}
                                    </button>
                                </form>
                                <div class="text-center mt-2">
                                    <b>OR</b>
                                </div>
                                <a class="mt-3 text-white btn btn-primary btn-flat btn-block">
                                        Create Account With Google
                                </a>
                                <p class="mt-2 mb-1">
                                    <a href="/">
                                        {{ __('Already with an account? Login') }}
                                    </a>
                                </p>
                            </div>
                            <!-- /.form-box -->
                        </div><!-- /.register-box -->
                    </div>
                </div>
                <div class="col-lg-4"></div>
            </div>

    </div>
        @stop

        @section('adminlte_js')
            <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
