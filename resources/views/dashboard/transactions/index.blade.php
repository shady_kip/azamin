<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/3/2020
 * Time: 4:04 PM
 */
?>

@extends('adminlte::page')

@section('title', 'Transactions')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Transactions</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="link-color" href="#">Home</a></li>
                    <li class="breadcrumb-item active">transaction</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
@stop

@section('content')
<div class="card">
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Trans ID</th>
                <th>Product Name</th>
                <th>Customer Name</th>
                <th>Total Amount</th>
                <th>Status</th>
                <th>Trans Date</th>
            </tr>
            </thead>
            <tbody>

            @foreach($transactions as $transaction)
                <tr>
                    <td>
                       {{uniqid()}}
                    </td>
                    <td>
                        {{$order->title}}
                    </td>
                    <td></td>

                    <td>

                    </td>

                    <td></td>
                    <td></td>

                </tr>
            @endforeach
        </table>
    </div>
    <!-- /.card-body -->
</div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/public/css/dataTables.bootstrap4.css">
@stop

@section('js')
    <script src="/js/jquery.dataTables.js"></script>
    <script src="/js/dataTables.bootstrap4.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
@stop

