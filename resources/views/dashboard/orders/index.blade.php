<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/3/2020
 * Time: 4:04 PM
 */
?>

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Orders</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="link-color" href="#">Home</a></li>
                    <li class="breadcrumb-item active">Orders</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
@stop

@section('content')
    <div class="card">
        <div class="card-header">

        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Product Image</th>
                    <th>Product Name</th>
                    <th>Customer Name</th>
                    <th>Price</th>
                    <th>Total Amount</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>

                @foreach($orders as $order)
                    <tr>
                        <td>
                            <img style="max-height: 55px"  src="{{asset("product_images/resized/$order->featured_image")}}">
                        </td>
                        <td>
                            {{$order->title}}
                        </td>
                        <td></td>
                        <td></td>
                        <td>

                        </td>
                        <td>
                            {{number_format($order->selling_price)}}
                        </td>
                        <td>
                            <a href="/dashboard/physical/edit/{{$order->id}}" class="btn btn-sm btn-success"><i  class="fa fa-edit"></i></a>
                            <a href="#" onclick="

                                    if(confirm('Are you sure')){
                                    $.ajax({
                                    method:'GET',
                                    url:'/product/destroy/{{$product->id}}',
                                    success:function(resp) {
                                    $('#row{{$product->id}}').hide();
                                    console.log(resp)
                                    }
                                    })
                                    }

                                    " class="btn btn-sm btn-danger">  <i  class="fa fa-trash"></i></a>
                        </td>

                    </tr>
                @endforeach
            </table>
        </div>
        <!-- /.card-body -->
    </div>

@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/public/css/dataTables.bootstrap4.css">
@stop

@section('js')
    <script src="/js/jquery.dataTables.js"></script>
    <script src="/js/dataTables.bootstrap4.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
@stop

