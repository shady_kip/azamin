<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/11/2020
 * Time: 12:22 PM
 */
?>
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="link-color" href="#">Home</a></li>
                    <li class="breadcrumb-item active">sub categories</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
@stop

@section('content')

<div class="card">
    <div class="card-body">
       <h2> {{\App\Category::find(request()->sub_id)->name}}</h2>
      <div class="row">
          <div class="col-lg-6">
              <button type="button" data-toggle="modal" data-target="#modal-category" class="btn btn-sm float-right btn-info">Add sub-category </button>
              <br/>
              <br/>
              <table class="table table-bordered">
                  <tr>
                      <th style="width: 10px">#</th>
                      <th>Sub category</th>

                      <th >Action</th>
                  </tr>
                  @foreach($categories as $key =>$category)

                  <tr id="row{{$category->id}}">
                      <td>{{$key+1}}</td>
                      <td>{{$category->name}}</td>

                      <td>
                          <button data-toggle="modal" data-target="#modal-sub-category{{$category->id}}" class="btn btn-sm btn-primary"><i class="fa fa-plus-square"></i></button>
                          <button
                                  onclick='

                                          if(confirm("Are you sure you want to delete?")){
                                          $.ajax({
                                          method:"get",
                                          url:"/physical/category/delete/{{$category->id}}",
                                          success:function(resp) {
                                          if(resp.message){
                                          $("#row{{$category->id}}").hide()

                                          }
                                          console.log(resp)
                                          }
                                          })
                                          }


                                          '

                                  class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> </button>
                      </td>
                  </tr>

                      <div  class="modal fade" id="modal-sub-category{{$category->id}}">
                          <div class="modal-dialog">
                              <div style="border-radius: 0px !important;" class="modal-content">
                                  <div class="modal-header">
                                      <h5 class="modal-title">{{$category->name}}</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                  </div>
                                  <form enctype="multipart/form-data"  method="post" action="/physical/category/create">
                                      <div class="modal-body">

                                          @csrf
                                          <div class="form-group">
                                              <label for="cat-name"> Name</label>
                                              <input name="category" class="form-control" type="text" id="cat-name" placeholder="category name">
                                              @if ($errors->has('name'))
                                                  <div class="invalid-feedback">
                                                      {{ $errors->first('name') }}
                                                  </div>
                                              @endif
                                          </div>

                                          <div class="form-group">
                                              <label> Category</label>
                                              <select name="parent" required class="form-control">
                                                  <option selected disabled>--select--</option>



                                                      <option  selected    value="{{$category->id}}">{{$category->name}}</option>

                                              </select>
                                          </div>



                                      </div>
                                      <div class="modal-footer justify-content-end">

                                          <button type="submit" class="btn float-right btn-flat btn-warning btn-primary">Save changes</button>
                                      </div>
                                  </form>
                              </div>
                              <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                      </div>
                      @endforeach

              </table>
          </div>
      </div>
    </div>
</div>
<div  class="modal fade" id="modal-category">
    <div class="modal-dialog">
        <div style="border-radius: 0px !important;" class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form enctype="multipart/form-data"  method="post" action="/physical/category/create">
                <div class="modal-body">

                    @csrf
                    <div class="form-group">
                        <label for="cat-name"> Name</label>
                        <input name="category" class="form-control" type="text" id="cat-name" placeholder="category name">
                        @if ($errors->has('name'))
                            <div class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label> Category</label>
                        <select name="parent" required class="form-control">
                            <option selected disabled>--select--</option>
                            @foreach(\App\Category::all() as $category)


                                <option @if(request()->sub_id==$category->id) selected @else style="display: none" @endif;   value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>



                </div>
                <div class="modal-footer justify-content-end">

                    <button type="submit" class="btn float-right btn-flat btn-warning btn-primary">Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<!-- /.modal -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop

