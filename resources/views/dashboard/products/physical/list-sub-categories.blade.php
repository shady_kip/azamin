@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Physical Products Sub-Categories</h3>
            <a href="#!" data-toggle="modal" data-target="#modal-category" class="btn btn-flat float-right btn-warning">Create Sub category&nbsp;<i class="fa fa-plus-circle"></i> </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Parent category</th>
                    <th>Name</th>
                    <th>Actions</th>

                </tr>
                </thead>
                <tbody>

                @foreach($subcategories as $sub)
                <tr>
                    <td>
                        {{$sub->categories[0]->name}}
                    </td>
                    <td>
                      {{$sub->name}}
                    </td>
                    <td>
                        <a href="#!" data-toggle="modal" data-target="#modal-category{{$sub->id}}"  class="btn btn-sm btn-success"><i  class="fa fa-edit"></i></a>
                        <a class="btn btn-sm btn-danger">  <i  class="fa fa-trash"></i></a>
                    </td>

                </tr>
                <div  class="modal fade" id="modal-category{{$sub->id}}">
                    <div class="modal-dialog">
                        <div style="border-radius: 0px !important;" class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Edit {{$sub->name}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="/dashboard/physical/sub-category/update/{{$sub->id}}" method="post">
                                <div class="modal-body">

                                    @csrf
                                    <div class="form-group">
                                        <label>Select Category</label>
                                        <select name="category_id" required class="form-control">
                                            <option selected disabled>--select--</option>
                                            @foreach(\App\Category::all() as $category)


                                                <option   @if($category->name==$sub->categories[0]->name) selected @endif value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="cat-name">Sub-Category Name</label>
                                        <input value="{{$sub->name}}" name="sub_name" required class="form-control" type="text" id="cat-name" placeholder="sub category name">
                                    </div>


                                </div>
                                <div class="modal-footer justify-content-end">

                                    <button type="submit" class="btn float-right btn-flat btn-warning btn-primary">Update changes</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                @endforeach
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <div  class="modal fade" id="modal-category">
        <div class="modal-dialog">
            <div style="border-radius: 0px !important;" class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create Sub-Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/dashboard/physical/sub-category" method="post">
                <div class="modal-body">

                        @csrf
                        <div class="form-group">
                            <label>Select Category</label>
                            <select name="category_id" required class="form-control">
                                <option selected disabled>--select--</option>
                                @foreach(\App\Category::all() as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                              @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="cat-name">Sub-Category Name</label>
                            <input name="sub_name" required class="form-control" type="text" id="cat-name" placeholder="sub category name">
                        </div>


                </div>
                <div class="modal-footer justify-content-end">

                    <button type="submit" class="btn float-right btn-flat btn-warning btn-primary">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/public/css/dataTables.bootstrap4.css">
@stop

@section('js')
    <script src="/js/jquery.dataTables.js"></script>
    <script src="/js/dataTables.bootstrap4.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
@stop
