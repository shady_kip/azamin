@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Physical Products</h3>
            <a href="/dashboard/physical/create-product"  class="btn btn-flat float-right btn-warning">Add New Product&nbsp;<i class="fa fa-plus-circle"></i> </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Product Image</th>
                    <th>Product Name</th>
                    <th>Category</th>
                    <th>Price&nbsp;(Ksh)</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>

                @foreach($products as $product)
                <tr id="row{{$product->id}}">
                    <td>
                        <img style="max-height: 55px"  src="{{asset("product_images/resized/$product->featured_image")}}">
                    </td>
                    <td>
                        {{$product->title}}
                    </td>
                    <td>
                        @if($product->category_id!==null)
                     {{\App\Category::find($product->category_id)->name}}
                            @else
                          -

                            @endif
                    </td>
                    <td>

                        @if($product->product_type=='variable-product')

                            {{ collect(\App\Variation::where('product_id',$product->id)->get())->min('price')}}  -   {{ collect(\App\Variation::where('product_id',$product->id)->get())->max('price')}}

                            {{--<span class="badge badge-secondary">Variable product</span>--}}

                            @else
                            {{number_format($product->selling_price)}}
                            @endif

                    </td>
                    <td>
                        <a class="btn btn-sm btn-info" href="/dashboard/physical/product/{{$product->slug}}"><i class="fa fa-eye"></i> </a>
                        <a href="/dashboard/physical/edit/{{$product->id}}" class="btn btn-sm btn-success"><i  class="fa fa-edit"></i></a>
                        <a href="#" onclick="

                        if(confirm('Are you sure')){
                            $.ajax({
                            method:'GET',
                            url:'/product/destroy/{{$product->id}}',
                                success:function(resp) {
                                $('#row{{$product->id}}').hide();
                                  console.log(resp)
                                }
                            })
                        }

                            " class="btn btn-sm btn-danger">  <i  class="fa fa-trash"></i></a>
                    </td>

                </tr>
           @endforeach
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <div  class="modal fade" id="modal-category">
        <div class="modal-dialog">
            <div style="border-radius: 0px !important;" class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create New Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label>Select Category</label>
                            <select class="form-control">
                                <option selected disabled>--select--</option>
                                <option>option 2</option>
                                <option>option 3</option>
                                <option>option 4</option>
                                <option>option 5</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="cat-name">Sub-Category Name</label>
                            <input class="form-control" type="text" id="cat-name" placeholder="category name">
                        </div>

                    </form>
                </div>
                <div class="modal-footer justify-content-end">

                    <button type="button" class="btn float-right btn-flat btn-warning btn-primary">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/public/css/dataTables.bootstrap4.css">
@stop

@section('js')
    <script src="/js/jquery.dataTables.js"></script>
    <script src="/js/dataTables.bootstrap4.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
@stop
