<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/13/2020
 * Time: 8:19 AM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/11/2020
 * Time: 3:04 PM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/11/2020
 * Time: 3:02 PM
 */
?>
@extends('adminlte::page')

@section('title', $product->title)

@section('content_header')

@stop

@section('content')


    <div class="card">
        <div class="card-header">
            <h5 class="card-title text-bold">{{$product->title}}</h5>
            @if($product->product_type=='variable-product')
            <a href="/dashboard/physical/product/variation/add/{{$product->id}}"  class="btn btn-flat btn-sm float-right btn-warning">Add Variation&nbsp;</a>
                @endif
        </div>
        <!-- /.card-header -->
        <div class="card-body">











            <div class="row">
                <div class="col-lg-6">
                    <div class="card" style="width: 100%;">

                        <div class="card-body">

                            <table class="table table-striped">

                                <tbody>
                                <br/>
                                <tr>
                                    <th scope="row">Item cost</th>
                                    <td>{{number_format($product->cost_price)}}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Selling price</th>
                                    <td>{{number_format($product->selling_price)}}</td>
                                </tr>

                                </tbody>
                            </table>

                        </div>

                        <div class="card-body">
                            <h5  class="card-title text-bold">Features</h5>
                            <p class="card-text">{!! $product->features !!}</p>
                            <h5  class="card-title text-bold">Description</h5>
                            <p class="card-text">{!! $product->description !!}</p>
                            <a href="#" class="card-link">Card link</a>
                            <a href="#" class="card-link">Another link</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <img  src="/product_images/original/{{$product->featured_image}}" class="card-img-top" alt="...">
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop



