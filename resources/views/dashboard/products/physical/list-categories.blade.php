@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Physical Product Categories</h3>
            <a href="#!" data-toggle="modal" data-target="#modal-variation" class="btn btn-flat float-right btn-info">Create Variation&nbsp;<i class="fa fa-plus-circle"></i> </a>
            <a href="#!" data-toggle="modal" data-target="#modal-category" class="btn btn-flat float-right btn-warning">Create new category&nbsp;<i class="fa fa-plus-circle"></i> </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Subcategories</th>
                    <th>Actions</th>

                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                <tr id="row{{$category->id}}">
                    <td>
                        @if($category->image!=='noimage.jpg')
                        <img style="max-height: 30px" src="{{asset('/category_images/'.$category->image)}}"/>
                            @else
                            <img style="max-height: 30px" src="{{asset('/images/placeholder.png')}}"/>
                        @endif
                    </td>
                    <td>
                       {{$category->name}}
                    </td>
                    <td>
                        @foreach($category->children as $item)
                            <small><a href="/dashboard/physical/category/sub/{{$item->id}}"  class="mt-3">
                                {{$item->name}}
                            </a>,</small>
                            @endforeach
                    </td>
                    <td >
                        <a data-toggle="modal" data-target="#modal-category{{$category->id}}"   href="#" class="btn btn-sm btn-success"><i  class="fa fa-edit"></i></a>
                        <a onclick='

                         if(confirm("Are you sure you want to delete?N/B this will delete all subcategories")){
                             $.ajax({
                             method:"get",
                             url:"/physical/category/delete/{{$category->id}}",
                            success:function(resp) {
                                 if(resp.message){
                                     $("#row{{$category->id}}").hide()

                            }
                            console.log(resp)
                            }
                             })
                         }


                       ' href="#!"  class="btn btn-sm btn-danger">  <i  class="fa fa-trash"></i></a>
                    </td>

                </tr>
{{--Edit category modal--}}
                <div  class="modal fade" id="modal-category{{$category->id}}">
                    <div class="modal-dialog">
                        <div style="border-radius: 0px !important;" class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Edit {{$category->name}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form enctype="multipart/form-data"  method="post" action="/physical/category/edit/{{$category->id}}">
                                <div class="modal-body">

                                    @csrf
                                    <div class="form-group">
                                        <label for="cat-name{{$category->id}}">Category Name</label>
                                        <input value="{{$category->name}}" name="cat_name" class="form-control" type="text" id="cat-name{{$category->id}}" placeholder="category name">
                                        @if ($errors->has('name'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="imageFile{{$category->id}}">Category Image <small>optional</small></label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input name="cat_image" type="file" class="custom-file-input" id="imageFile{{$category->id}}">
                                                <label class="custom-file-label" for="imageFile">Choose file</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="">Upload</span>
                                            </div>
                                        </div>
                                        <br/>
                                        <img style="max-height: 30px" src="{{asset('/category_images/'.$category->image)}}"/>
                                    </div>

                                </div>
                                <div class="modal-footer justify-content-end">

                                    <button type="submit" class="btn float-right btn-flat btn-warning btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                @endforeach;


            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

    {{--variations model--}}
    <!-- /.card -->
    <div  class="modal fade" id="modal-variation">
        <div class="modal-dialog">
            <div style="border-radius: 0px !important;" class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create Category variation</h5>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form enctype="multipart/form-data"  method="post" action="/physical/category/variation/create">
                    <div class="modal-body">
                        <small>Note these are global attributes that are common to a category</small>

                        @csrf

                        <div class="form-group">
                            <label>Select Category *</label>
                            <select name="parent" required="required" class="form-control">
                                <option selected disabled>--select--</option>
                                @foreach($categories as $category)


                                    <option  value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="cat-name"> Attribute name</label>
                            <br/>
                            <input name="attribute" class="form-control" type="text" id="cat-attribute" placeholder="Attribute e.g size, memory etc">
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="variation">Value(s)</label>
                            <br/>
                            <small>Enter comma separated values</small>
                            <textarea name="var_values" class="form-control" id="variation" rows="2"></textarea>
                        </div>



                        <div class="form-group">
                            <label for="imageFile">Category Image <small>optional</small></label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input name="cat_image" type="file" class="custom-file-input" id="imageFile">
                                    <label class="custom-file-label" for="imageFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="">Upload</span>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer justify-content-end">

                        <button type="submit" class="btn float-right btn-flat btn-warning btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div  class="modal fade" id="modal-category">
        <div class="modal-dialog">
            <div style="border-radius: 0px !important;" class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create New Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form enctype="multipart/form-data"  method="post" action="/physical/category/create">
                <div class="modal-body">

                        @csrf
                        <div class="form-group">
                            <label for="cat-name"> Name</label>
                            <input name="category" class="form-control" type="text" id="cat-name" placeholder="category name">
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>

                    <div class="form-group">
                        <label>Select Category</label>
                        <select name="parent" required class="form-control">
                            <option selected disabled>--select--</option>
                            @foreach(\App\Category::all() as $category)


                                <option  value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>

                        <div class="form-group">
                            <label for="imageFile">Category Image <small>optional</small></label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input name="cat_image" type="file" class="custom-file-input" id="imageFile">
                                    <label class="custom-file-label" for="imageFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="">Upload</span>
                                </div>
                            </div>

                        </div>

                </div>
                <div class="modal-footer justify-content-end">

                    <button type="submit" class="btn float-right btn-flat btn-warning btn-primary">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/public/css/dataTables.bootstrap4.css">
@stop

@section('js')
    <script src="/js/jquery.dataTables.js"></script>
    <script src="/js/dataTables.bootstrap4.js"></script>
    <script>
        $("#example1").DataTable();
        function editCategory(cat_id) {
            alert(cat_id)
        }
    </script>
@stop
