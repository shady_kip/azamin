<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/13/2020
 * Time: 4:35 PM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/11/2020
 * Time: 3:04 PM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/11/2020
 * Time: 3:02 PM
 */
?>
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')


    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Add Attributes</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form method="post" action="/dashboard/physical/product/attributes/create" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <div  class="variation_form">
                        <label>Attribute</label>
                        <br/>
                        <small>Add product attribute</small>

                        <div class="form-group">
                            <label for="attribute_name">Attribute name</label>
                            <input required name="att_name" id="variation_name" type="text" class="form-control">
                        </div>
                        <button  type="submit" class="btn btn-flat btn-secondary btn-sm">Save</button>
                    </div>
                    <br/>

                    <button style="display: none" onclick="addFields()" type="button" class="btn btn-warning btn-flat"><i class="fa fa-plus"></i>&nbsp;Add options set </button>

                </div>
            </form>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach(\App\Attribute::all() as $key=>$attr)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td>{{$attr->name}}</td>
                        <td>
                            <a href="#!" class="btn btn-sm btn-success"><i class="fa fa-plus-square"></i> </a>
                        </td>

                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop



