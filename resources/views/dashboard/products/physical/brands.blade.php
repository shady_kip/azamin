<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/11/2020
 * Time: 2:52 PM
 */
?>
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Brands</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="link-color" href="#">Home</a></li>
                    <li class="breadcrumb-item active">brands</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Popular brands</h3>
            <a href="/dashboard/brands/create"  class="btn btn-flat float-right btn-warning">Add Brand&nbsp;<i class="fa fa-eye"></i> </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Logo</th>
                    <th>Name</th>

                    <th>Actions</th>

                </tr>
                </thead>
                <tbody>
                @foreach($brands as $brand)
                    <tr id="row{{$brand->id}}">
                        <td>
                            @if($brand->logo!=='noimage.jpg')
                                <img style="max-height: 30px" src="{{asset('/brands/resized/'.$brand->logo)}}"/>
                            @else
                                <img style="max-height: 30px" src="{{asset('/images/placeholder.png')}}"/>
                            @endif
                        </td>
                        <td>
                            {{$brand->name}}
                        </td>

                        <td >
                            <a data-toggle="modal" data-target="#modal-category{{$brand->id}}"   href="#" class="btn btn-sm btn-success"><i  class="fa fa-edit"></i></a>
                            <a onclick='

                                    if(confirm("Are you sure you want to delete?")){
                                    $.ajax({
                                    method:"get",
                                    url:"/dashboard/brands/destroy/{{$brand->id}}",
                                    success:function(resp) {
                                    if(resp.message){
                                    $("#row{{$brand->id}}").hide()

                                    }
                                    console.log(resp)
                                    }
                                    })
                                    }


                                    ' href="#!"  class="btn btn-sm btn-danger">  <i  class="fa fa-trash"></i></a>
                        </td>

                    </tr>
                    {{--Edit category modal--}}
                    <div  class="modal fade" id="modal-category{{$brand->id}}">
                        <div class="modal-dialog">
                            <div style="border-radius: 0px !important;" class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Edit {{$brand->name}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form enctype="multipart/form-data"  method="post" action="{{route('brands.update',$brand->id)}}">
                                    <div class="modal-body">

                                        @csrf
                                        <div class="form-group">
                                            <label for="brand-name{{$brand->id}}">Brand Name</label>
                                            <input value="{{$brand->name}}" name="brand" class="form-control" type="text" id="brand-name{{$brand->id}}" placeholder="category name">
                                            @if ($errors->has('name'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('name') }}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="imageFile{{$brand->id}}">Brand Logo <small>optional</small></label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input name="brand_logo" type="file" class="custom-file-input" id="imageFile{{$brand->id}}">
                                                    <label class="custom-file-label" for="imageFile">Choose file</label>
                                                </div>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="">Upload</span>
                                                </div>
                                            </div>
                                            <br/>
                                            <img style="max-height: 30px" src="{{asset('/brands/resized/'.$brand->logo)}}"/>
                                        </div>

                                    </div>
                                    <div class="modal-footer justify-content-end">

                                        <button type="submit" class="btn float-right btn-flat btn-warning btn-primary">Save changes</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                @endforeach


            </table>
        </div>
    </div>
@stop

@section('content')

    <!-- /.row -->

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop


