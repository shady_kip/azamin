@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div style="display: none"  class="loading_overlay">
        <div class="text-center loading-text"></div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Edit</h3>
            <a href="/dashboard/physical/product-lists"  class="btn btn-flat float-right btn-warning">Add Products&nbsp;<i class="fa fa-eye"></i> </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form onsubmit="return false;" enctype="multipart/form-data" id="product_form">
                <input id="_token" value="{{csrf_token()}}" type="hidden">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input value="{{$product->title}}" required id="title" placeholder="product title" name="product_title" class="form-control" type="text">
                </div>

                <div class="form-group">
                    <label for="features">Features</label>
                    <textarea name="features" id="features" class="form-control">
                        {{$product->features}}
                    </textarea>
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea    id="description" class="form-control">
                        {{$product->description}}
                    </textarea>
                </div>

                <div class="form-group">
                    <label>Select Category &nbsp;or&nbsp;<small><a href="/dashboard/physical/category" >Add new category</a> </small></label>
                    <br/>
                    <b>current category</b>
                    @if(isset($product->category_id))
                    {{\App\Category::find($product->category_id)->name}}
                    @endif
                    <select required id="category" name="category" onchange="fetchSubcat()" class="form-control">
                        <option selected disabled>--select--</option>

                        @foreach($categories as $cat)
                            <optgroup label="{{$cat->name}}">
                                @foreach($cat->children as $item)
                                    <option @if($product->category_id==$item->id) selected @endif value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="sub-category">Select sub-category </label>
                    <small>select category first</small>
                    <select class="form-control" name="sub_category" id="sub-category">
                      <option></option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="brand">Brand </label>

                    <select class="form-control" name="brand" id="brand">

                        <option disabled selected>--select-brand</option>
                        @foreach(\App\Brand::all() as $brand)
                            <option @if($product->brand_id==$brand->id) selected @endif  value="{{$brand->id}}">{{$brand->name}}</option>
                        @endforeach

                    </select>
                </div>

                <div class="form-check">
                    <input @if($product->featured==1) checked @endif  name="featured" value="1" type="checkbox" class="form-check-input" id="isFeatured">
                    <label class="form-check-label" for="isFeatured">Featured?</label>
                </div>
                <div class="form-group">
                    <label for="product_sku">Product SKU</label>
                    <input value="{{$product->SKU}}" id="product_sku" placeholder="Product SKU" name="product_sku" class="form-control" type="text">
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-6">
                            <label for="cp">Item Cost</label>
                            <input value="{{$product->cost_price}}" id="cp" placeholder="Cost Price" name="product_cp" class="form-control" type="number">
                        </div>
                        <div class="col-lg-6">
                            <label for="sp">Selling Price</label>
                            <input value="{{$product->selling_price}}" required id="sp" placeholder="Selling Price" name="product_sp" class="form-control" type="number">
                        </div>
                    </div>

                </div>
                <br/>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Variations</label>
                            <br/>
                            <small>Add product variations</small>
                            <div style="display: none" class="variation_form">
                                <div class="form-group">
                                    <label for="variation_name">Variation name</label>
                                    <input name="variation_name" id="variation_name" type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="variation_value">Variation Value</label>
                                    <input id="variation_value" name="variation_value" type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="variation_price">Price<small>optional</small></label>
                                    <input id="variation_price" name="variation_price" type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="imageFile"> Image <small>optional</small></label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input name="variation_image" type="file" class="custom-file-input" id="variation_image">
                                            <label class="custom-file-label" for="variation_image">Choose file</label>
                                        </div>
                                    </div>

                                </div>
                                <button onclick="save_variation_to_db('{{$product->id}}')" type="button" class="btn btn-flat btn-secondary btn-sm">Save</button>
                            </div>
                            <br/>
                            <button onclick="addFields()" type="button" class="btn btn-warning btn-flat"><i class="fa fa-plus fa-2x"></i> </button>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <td>Variation Name</td>
                                <td>Variation Value</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach(\App\Variation::where('product_id',$product->id)->get() as $variation)

                            <tr id="row{{$variation->id}}">
                                <td>
                                    {{$variation->name}}
                                </td>
                                <td>
                                    {{$variation->variation_value}}
                                </td>
                                <td>
                                    <a onclick="removeVariation('{{$variation->id}}')" class="btn btn-sm btn-danger" href="#!"><i class="fa fa-trash"></i> </a>
                                </td>

                            </tr>
                                @endforeach

                            </tbody>
                            <tbody id="variation_res">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="prod_id"></div>
                <br/>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="featured_image">Featured Image</label>
                            <div class="img-browser">
                                <img  style="height: 250px"  src="{{asset("product_images/resized/$product->featured_image")}}" id="preview" >
                            </div>
                            <br>
                            <input style="display: none" type="file" name="featured_image" class="file featured_img" accept="image/*">
                        </div>
                        <div id="prod_id"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="other_images">Other Images</label>
                            <small>Add other  products images</small>
                            <br/>
                            <div
                                class="dropzone" id="product_images">
                            </div>
                        </div>

                        @foreach(\App\Gallery::where('product_id',$product->stock_id)->get() as $photo)
                        <div class="media">
                            <img src="{{asset("product_images/resized/$photo->name")}}" class="mr-3" alt="...">
                            <div class="media-body">
                                <div class="form-check">
                                    <input
                                        id="gallery{{$photo->id}}"
                                        value="{{$photo->name}}"
                                        class="form-check-input"
                                        type="radio" name="radio"

                                    >
                                    <label for="gallery{{$photo->id}}" class="form-check-label" >
                                        Set as featured
                                    </label>
                                </div>
                            </div>
                        </div>
                     @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <button onclick="submitData()" type="submit" class="btn btn-warning btn-flat">Update Product</button>
                </div>
            </form>

        </div>
    </div>

    <!-- /.card -->
    <div  class="modal fade" id="modal-category">
        <div class="modal-dialog">
            <div style="border-radius: 0px !important;" class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create New Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form onsubmit="return false" enctype="multipart/form-data"  method="post" action="/physical/category/create">
                    <div class="modal-body">

                        @csrf
                        <div class="form-group">
                            <label for="cat-name">Category Name</label>
                            <input name="cat_name" class="form-control" type="text" id="cat-name" placeholder="category name">
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="sub-name">Sub category name</label>
                            <input id="sub-name" type="text" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="imageFile">Category Image <small>optional</small></label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input name="cat_image" type="file" class="custom-file-input" id="imageFile">
                                    <label class="custom-file-label" for="imageFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="">Upload</span>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer justify-content-end">

                        <button onclick="create_category()" type="button" class="btn float-right btn-flat btn-warning btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@stop

@section('css')
    <style>
        .loading_overlay{
            background-color: rgba(0,0,0,.6);
            height: 100vh;
            position: fixed;
            left: 0 !important;
            top: 0 !important;
            width: 100%;
            z-index: 100000 !important;
            display: flex;
            justify-content: center;
            align-items: center;
            -webkit-transition: opacity 2s ease-in;
            -moz-transition: opacity 2s ease-in;
            -o-transition: opacity 2s ease-in;
            transition: opacity 2s ease-in !important;

        }
        .loading_overlay>div{
            color: white !important;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>

@stop

@section('js')
    <script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script>

    <script>

        let uniq_prod_id="{{$product->stock_id}}";
        var variations = [];
        let description_editor;
        let featured_editor;
        let product_id;
        let isLoading=false;
        let isFormSubmitted=false;





        ClassicEditor
            .create(document.querySelector('#description'))
            .then( newEditor => {
                description_editor = newEditor;
            } )
            .catch(error=>{
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#features'))
            .then( newEditor => {
                featured_editor = newEditor;
            } )
            .catch(error=>{
                console.error(error);
            });


        function create_category(){
            let cat_name  = $('#cat-name').val();
            let useAsCat =$('#useAsCat').val();
            var formData = new FormData();
            formData.append('cat_image',$('input[name=cat_image]')[0].files[0]);
            formData.append('cat_name',cat_name);
            formData.append('added_from_product_page','1');
            formData.append('sub_name',$('#sub-name').val());
            formData.append('_token',$('#_token').val());

            $.ajax({
                method:'post',
                data:formData,
                enctype: 'multipart/form-data',
                url:'/physical/category/create',
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+) //thanks to stack overflow
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function (resp) {

                    $('#modal-category').modal('hide');
                    document.querySelector("#category").innerHTML=resp;
                    console.log(resp)
                },
                error: function (error) {
                    console.log(error)
                }
            })
        }

        function removeVariation(id){

           if(confirm("Are sure?")){
               $.ajax({
                   method:'GET',
                   url:'/product/variation/destroy/'+id,
                   success:function (results) {
                       $('#row'+id).hide()
                   }
               })
           }
        }

        $(".img-browser").on("click", function() {
            var file = $(this).parent().find(".file");

            file.trigger("click");
        });
        $('.featured_img').change(function(e) {
            var fileName = e.target.files[0].name;
            $("#file").val(fileName);

            var reader = new FileReader();
            reader.onload = function(e) {
                // get loaded data and render thumbnail.
                document.getElementById("preview").src = e.target.result;
            };
            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        });

        function addFields(){
            $('.variation_form').show();
        }

        function fetchSubcat(){
            let catValue = $('#category').val();
            let el =document.querySelector("#sub-category");
            if(catValue!==''){
                $.ajax({
                    method:'GET',
                    url: '/getSubcategory/'+catValue,
                    success:function (data) {
                        el.innerHTML=data;
                    }

                })
            }

        }



        var myDropzone = new Dropzone("div#product_images",
            {
                maxFilesize: 12,
                url:'/images-save/'+uniq_prod_id,
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                autoProcessQueue:false,
                addRemoveLinks: true,
                timeout: 0,
                success: function(file, response)
                {
                    console.log(response);
                },
                error: function(file, response)
                {
                    return false;
                }
            }
        );
        //script to store products
        function submitData(e){

            let title = $('#title').val();
            let description =description_editor.getData();
            let features = featured_editor.getData();
            let category = $('#category').val();

            let subcategory = $('#sub-category').val();
            let featured = $('#isFeatured').val();
            let product_sku = $('#product_sku').val();
            let cost_price = $('#cp').val();
            let selling_price = $('#sp').val();




            let brand_id = $('#brand').val();
            let _token = $('#_token').val();
            let product_variations = JSON.stringify(variations);

            let product =  {
                title,
                description,
                features,
                category_id:null,
                brand_id,
                subcategory,
                featured,
                product_sku,
                cost_price,
                selling_price,
                _token,
                product_variations,

                stock_id:uniq_prod_id
            };
            if($('#sub_category').val()==null){
                product.category_id = $('#category').val();
            }else{
                product.category_id = $('#sub_category').val();
            }

            if(selling_price!==''){
                $(".loading_overlay").show();
                document.querySelector('.loading-text').innerHTML="Creating product please wait....";
                $.ajax({
                    method: 'POST',
                    url:"/dashboard/physical/product/update/{{$product->id}}",
                    data:product,
                    success:function (resp) {
                        if(resp.message){
                            uploadImages(resp.product.id);

                        }
                    },
                    error:function (error) {
                        console.log(error)
                    }
                })
            }else{
                alert("Provide price for the product")
            }
        }


        function   uploadImages(id){
            product_id=id;
            document.querySelector('.loading-text').innerHTML="Uploading images,we will be done soon....";

            document.querySelector('#prod_id').innerHTML=id;


            var formData = new FormData();
            formData.append('featured_image',$('input[name=featured_image]')[0].files[0]);
            formData.append('id',id);
            formData.append('_token',$('#_token').val());
            $.ajax({
                method:'post',
                data:formData,
                enctype: 'multipart/form-data',
                url:'/dashboard/physical/upload-image',
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+) //thanks to stack overflow
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function (resp) {

                    myDropzone.processQueue();
                    document.querySelector('.loading-text').innerHTML="Product saved";

                    setTimeout(()=>{
                        $('.loading_overlay').hide();

                    },1000)
                    $('.loading_overlay').hide();
                    console.log(resp)
                },
                error: function (error) {
                    console.log(error)
                }
            })
        }

        function save_variation_to_db(id){
            let name =$('#variation_name').val();
            let var_value = $('#variation_value').val();
            let var_price = $('#variation_price').val();
            let _token  = $('#_token').val();
            let image = $('input[name=variation_image]')[0].files[0];
            let formData = new FormData();
            formData.append('name',name.toUpperCase());
            formData.append('var_value',var_value.toUpperCase());
            formData.append('var_price',var_price);
            formData.append('_token',_token);
            if(image!==undefined) {
                formData.append('var_image', image);
            }
            $(".loading_overlay").show();
            document.querySelector('.loading-text').innerHTML="Saving variations....";
            $.ajax({
                method:'post',
                data:formData,
                enctype: 'multipart/form-data',
                url:'/product/variation/create/'+id,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+) //thanks to stack overflow
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function (resp) {
                    let output = `
                    <tr id="row${resp.variation.id}">
                    <td>
                   ${resp.variation.name}
                    </td>
                    <td>
                    ${resp.variation.value}
                    </td>
                    <td>
                                    <a onclick="removeVariation('"+resp.variation.id+"')" class="btn btn-sm btn-danger" href="#!"><i class="fa fa-trash"></i> </a>
                                </td>
                                </td>
                    `
                    $('#variation_res').append(output);
                    $(".loading_overlay").hide();
                },
                error: function (error) {
                    console.log(error)
                }
            })



        }
    </script>
@stop
