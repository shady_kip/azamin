<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/13/2020
 * Time: 2:12 PM
 */?>
<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/11/2020
 * Time: 3:04 PM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/11/2020
 * Time: 3:02 PM
 */
?>
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')


    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Edit</h3>
            <a href="/dashboard/physical/product-lists"  class="btn btn-flat float-right btn-warning">View Products&nbsp;<i class="fa fa-eye"></i> </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-lg-4">

                    <form class="needs-validation"  action="/dashboard/physical/product/variation/create/{{$product_id}}"  enctype="multipart/form-data" id="attForm" method="post">
                        <div class="form-group">
                            <label for="validationTooltip04">Select Attribute</label>
                            <select name="att_name" class="custom-select" id="validationTooltip04" required>
                                <option selected disabled value="">Choose...</option>
                                @foreach(\App\Attribute::all() as $attribute)
                                    <option value="{{$attribute->name}}">{{$attribute->name}}</option>
                                    @endforeach
                            </select>
                            <div class="invalid-tooltip">
                                Please select a valid atrribute.
                            </div>
                        </div>

                        @csrf

                        <div class="form-group">
                            <label for="att_value">Value (*)</label>
                            <input name="att_value" id="att_value" type="text" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="att_price">Price</label>
                            <input name="att_price" id="att_price" type="text" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="att_symbol">Symbol</label>
                            <input name="att_symbol" id="att_symbol" type="text" class="form-control">
                        </div>

                    <div class="form-group">
                        <button  type="submit" class="btn btn-flat btn-secondary btn-sm">Save</button>
                    </div>
                    </form>


                    </div>

                <div class="col-lg-8">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Value</th>
                            <th scope="col">price</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Variation::where('product_id',$product_id)->get() as $key=>$variant)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$variant->name}}</td>
                            <td>{{$variant->variation_value}}</td>
                            <td>{{$variant->price}}</td>
                        </tr>
                            @endforeach

                        </tbody>
                    </table>

                    {{--<table class="table table-striped">--}}
                        {{--<thead>--}}
                        {{--<tr>--}}
                            {{--<td>Attribute</td>--}}
                            {{--<td>Variation </td>--}}
                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody id="variation_res">--}}

                        {{--</tbody>--}}
                    {{--</table>--}}

                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
   <script>
       function addFields(){
           $('.variation_form').show();
       }
       function addAttFields() {
           $('#attForm').show()
       }
       function save_variation() {
           let variation_names = {
               name:'',
               var_value:'',
               var_price:''
           };
           let name = $('#variation_name').val();
           let var_value = $('#variation_value').val();
           let var_price = $('#variation_price').val();
           variation_names.name=name.toUpperCase();
           variation_names.var_value=var_value.toUpperCase();
           variation_names.var_price=var_price;

           let output = `
                      <tr>
                            <td>${variation_names.name}</td>
                            <td>${variation_names.var_value}</td>
                     </tr>
            `;
           $('#variation_res').append(output);
           variations.push(variation_names);
           $('#variation_value').val('');
           $('#variation_name').val('');
       }
   </script>
@stop



