@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div style="display: none"  class="loading_overlay">
        <div class="text-center loading-text"></div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Physical Products</h3>
            <a href="/dashboard/physical/product-lists"  class="btn btn-flat float-right btn-warning">View Products&nbsp;<i class="fa fa-eye"></i> </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
       <form onsubmit="return false;" enctype="multipart/form-data" id="product_form">
           <input id="_token" value="{{csrf_token()}}" type="hidden">
           <div class="form-group">
               <label for="title">Title</label>
               <input required id="title" placeholder="product title" name="product_title" class="form-control" type="text">
           </div>
           <div class="form-group">
               <label>Select Category &nbsp;or&nbsp;<small><a href="/dashboard/physical/category" >Add new category</a> </small></label>
               <select required id="category" name="category" onchange="fetchSubcat()" class="form-control">
                   <option selected disabled>--select--</option>

                   @foreach($categories as $cat)
                       <optgroup label="{{$cat->name}}">
                           @foreach($cat->children as $item)
                           <option value="{{$item->id}}">{{$item->name}}</option>
                           @endforeach
                       </optgroup>
                   @endforeach
               </select>
           </div>
           <div class="form-group">
               <label for="sub_category">Select sub-category </label>
               <small>select category first</small>
               <select class="form-control" name="sub_category" id="sub_category">

               </select>
           </div>
           <div class="form-group">
               <label for="description">Long description</label>
               <textarea   id="description" class="form-control"></textarea>
           </div>
           <div class="form-group">
               <label for="features">Features</label>
               <textarea name="features" id="features" class="form-control"></textarea>
           </div>

           <div class="form-group">
               <label for="brand">Select brand </label>

               <select class="form-control" name="brand" id="brand">

                   <option disabled selected>--select-brand</option>
                   @foreach(\App\Brand::all() as $brand)
                       <option value="{{$brand->id}}">{{$brand->name}}</option>
                       @endforeach

               </select>
           </div>

           <div class="form-check">
               <input name="featured" value="featured" type="checkbox" class="form-check-input" id="isFeatured">
               <label class="form-check-label" for="isFeatured">Featured?</label>
           </div>
           <div class="form-group">
               <label for="product_sku">Product SKU</label>
               <input  id="product_sku" placeholder="Product SKU" name="product_sku" class="form-control" type="text">
           </div>

           <div class="form-group">
               <label for="productType">Product Type</label>
               <select onchange="showVariation()" name="product-type"  class="form-control" id="productType">
                   <option selected disabled>--select--</option>
                   <option value="simple-product">Simple Product</option>
                   <option value="variable-product">Variable Product</option>
               </select>
           </div>

           <div id="price_" style="display: none" class="form-group">
               <div class="row">
                   <div class="col-lg-6">
                       <label for="cp">Cost Price</label>
                       <input id="cp" placeholder="Cost Price" name="product_cp" class="form-control" type="number">
                   </div>
                   <div class="col-lg-6">
                       <label for="sp">Selling Price</label>
                       <input required id="sp" placeholder="Selling Price" name="product_sp" class="form-control" type="number">
                   </div>
               </div>

           </div>
           <br/>






           <div id="variation_area" style="display: none;">

               <div class="alert alert-info" role="alert">
                   You will have to add options to the product in the product details page after saving this product!
               </div>

           </div>
        <div id="prod_id"></div>
           <br/>
               <div class="row">
                   <div class="col-lg-6">
                       <div class="form-group">
                           <label for="featured_image">Featured Image</label>
                           <div class="img-browser">
                               <img  style="height: 250px"  src="{{asset('/images/placeholder.png')}}" id="preview" >
                           </div>
                           <br>
                           <input style="display: none" type="file" name="featured_image" class="file featured_img" accept="image/*">
                           </div>
                       <div id="prod_id"></div>
                   </div>
                   <div class="col-lg-6">
                       <div class="form-group">
                           <label for="other_images">Other Images</label>
                           <small>Add other  products images</small>
                           <br/>
                           <div
                                 class="dropzone" id="product_images">
                           </div>
                       </div>
                   </div>
               </div>
           <div class="form-group">
               <button onclick="submitData()" type="submit" class="btn btn-warning btn-flat">Add Product</button>
           </div>
       </form>

        </div>
    </div>

    <!-- /.card -->
    <div  class="modal fade" id="modal-category">
        <div class="modal-dialog">
            <div style="border-radius: 0px !important;" class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create New Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form onsubmit="return false" enctype="multipart/form-data"  method="post" action="/physical/category/create">
                    <div class="modal-body">

                        @csrf
                        <div class="form-group">
                            <label for="cat-name">Category Name</label>
                            <input name="cat_name" class="form-control" type="text" id="cat-name" placeholder="category name">
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="sub-name">Sub category name</label>
                            <input id="sub-name" type="text" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="imageFile">Category Image <small>optional</small></label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input name="cat_image" type="file" class="custom-file-input" id="imageFile">
                                    <label class="custom-file-label" for="imageFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="">Upload</span>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer justify-content-end">

                        <button onclick="create_category()" type="button" class="btn float-right btn-flat btn-warning btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@stop

@section('css')
    <style>
        .loading_overlay{
            background-color: rgba(0,0,0,.6);
            height: 100vh;
            position: fixed;
            left: 0 !important;
            top: 0 !important;
            width: 100%;
            z-index: 100000 !important;
            display: flex;
            justify-content: center;
            align-items: center;
            -webkit-transition: opacity 2s ease-in;
            -moz-transition: opacity 2s ease-in;
            -o-transition: opacity 2s ease-in;
            transition: opacity 2s ease-in !important;

        }
        .loading_overlay>div{
            color: white !important;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>

@stop

@section('js')
    <script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script>

    <script>

        let uniq_prod_id="<?php
            $num = new \App\Helpers();
            echo $num->generate_uniq_id();

        ?>";
        var variations = [];
        let description_editor;
        let featured_editor;
        let product_id;
        let isLoading=false;
        let isFormSubmitted=false;





        ClassicEditor
            .create(document.querySelector('#description'))
            .then( newEditor => {
                description_editor = newEditor;
            } )
            .catch(error=>{
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#features'))
            .then( newEditor => {
                featured_editor = newEditor;
            } )
            .catch(error=>{
                console.error(error);
            });


        function create_category(){
           let cat_name  = $('#cat-name').val();
           let useAsCat =$('#useAsCat').val();
            var formData = new FormData();
            formData.append('cat_image',$('input[name=cat_image]')[0].files[0]);
            formData.append('cat_name',cat_name);
            formData.append('added_from_product_page','1');
            formData.append('sub_name',$('#sub-name').val());
            formData.append('_token',$('#_token').val());

            $.ajax({
                method:'post',
                data:formData,
                enctype: 'multipart/form-data',
                url:'/physical/category/create',
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+) //thanks to stack overflow
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function (resp) {

                    $('#modal-category').modal('hide');
                    document.querySelector("#category").innerHTML=resp;
                    console.log(resp)
                },
                error: function (error) {
                    console.log(error)
                }
            })
        }
        $(".img-browser").on("click", function() {
            var file = $(this).parent().find(".file");

            file.trigger("click");
        });
        $('.featured_img').change(function(e) {
            var fileName = e.target.files[0].name;
            $("#file").val(fileName);

            var reader = new FileReader();
            reader.onload = function(e) {
                // get loaded data and render thumbnail.
                document.getElementById("preview").src = e.target.result;
            };
            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        });





        function fetchSubcat(){
            let catValue = $('#category').val();
            console.log(catValue)
            let el =document.querySelector("#sub_category");
            if(catValue!==''){
                $.ajax({
                    method:'GET',
                    url: '/getSubcategory/'+catValue,
                    success:function (data) {
                       el.innerHTML=data;
                    }

                })
            }

        }



        var myDropzone = new Dropzone("div#product_images",
            {
                maxFilesize: 12,
                url:'/images-save/'+uniq_prod_id,
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                autoProcessQueue:false,
                parallelUploads:10,
                addRemoveLinks: true,
                timeout: 0,
                success: function(file, response)
                {
                    console.log(response);
                },
                error: function(file, response)
                {
                    return false;
                }
            }
        );
        //script to store products
        function submitData(e){

           let title = $('#title').val();
           let description =description_editor.getData();
           let features = featured_editor.getData();
           let category = $('#category').val();

           let subcategory = $('#sub-category').val();
           let featured = $('#isFeatured').val();
           let product_sku = $('#product_sku').val();
           let cost_price = $('#cp').val();
           let selling_price = $('#sp').val();
           let productType = $('#productType').val();




           let brand_id = $('#brand').val();
           let _token = $('#_token').val();
           let product_variations = JSON.stringify(variations);

            let product =  {
                title,
                description,
                features,
                category_id:null,
                brand_id,
                subcategory,
                productType,
                featured,
                product_sku,
                cost_price,
                selling_price,
                _token,
                product_variations,

                stock_id:uniq_prod_id
            };

            if($('#sub_category').val()==null){
                product.category_id = $('#category').val();
            }else{
                product.category_id = $('#sub_category').val();
            }

           if(title!==''){
               $(".loading_overlay").show();
               document.querySelector('.loading-text').innerHTML="Creating product please wait....";
               $.ajax({
                   method: 'POST',
                   url:"/dashboard/physical/create-product",
                   data:product,
                   success:function (resp) {
                       if(resp.message){
                       //    save_variation_to_db(resp.product.id);
                           uploadImages(resp.product.id);


                       }
                   },
                   error:function (error) {
                       console.log(error)
                   }
               })
           }else{
               alert("Provide title of the product")
           }
        }


       function   uploadImages(id){
            product_id=id;
           document.querySelector('.loading-text').innerHTML="Uploading images,we will be done soon....";

            document.querySelector('#prod_id').innerHTML=id;


          var formData = new FormData();
          formData.append('featured_image',$('input[name=featured_image]')[0].files[0]);
          formData.append('id',id);
          formData.append('_token',$('#_token').val());
          $.ajax({
              method:'post',
              data:formData,
              enctype: 'multipart/form-data',
              url:'/dashboard/physical/upload-image',
              contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+) //thanks to stack overflow
              processData: false, // NEEDED, DON'T OMIT THIS
              success: function (resp) {

                  myDropzone.processQueue();
                  document.querySelector('.loading-text').innerHTML="Product saved";

                  setTimeout(()=>{
                      $('.loading_overlay').hide();

                  },1000)
                  $('.loading_overlay').hide();
                  console.log(resp)
              },
              error: function (error) {
                  console.log(error)
              }
          })
      }

        function save_variation_to_db(id){
            let _token  = $('#_token').val();
            let formData = new FormData();
            formData.append('_token',_token);
          //  formData.append('var_image',$('input[name=variation_image]')[0].files[0]);
            $(".loading_overlay").show();
            document.querySelector('.loading-text').innerHTML="Saving variations....";
            $.ajax({
                method:'post',
                data:formData,
                enctype: 'multipart/form-data',
                url:'/product/variation/create/'+id,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+) //thanks to stack overflow
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function (resp) {
                    $(".loading_overlay").hide();
                },
                error: function (error) {
                    console.log(error)
                }
            })

        }
        function showVariation() {
            let type = $('#productType').val()
            if(type=='variable-product'){
                $('#variation_area').show()
                $('#price_').hide()


            }else{
                $('#variation_area').hide()
                $('#price_').show()
            }
        }
    </script>
@stop
