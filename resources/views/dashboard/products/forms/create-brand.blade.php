<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/11/2020
 * Time: 3:02 PM
 */
?>
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')


    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Brands</h3>
            <a href="/dashboard/brands/create"  class="btn btn-flat float-right btn-warning">View Brands&nbsp;<i class="fa fa-eye"></i> </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <!-- form start -->
                    <form enctype="multipart/form-data" action="{{route('brands.store')}}" method="post" role="form">
                        @csrf()
                        <div class="box-body">
                            <div class="form-group">
                                <label for="brand-name">Brand Name</label>
                                <input name="brand" type="text" class="form-control" id="brand-name" placeholder="Brand Name">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">logo</label>
                                <input name="logo_image" accept="image/*" type="file" id="exampleInputFile">
                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6"></div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop

