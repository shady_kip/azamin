<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/3/2020
 * Time: 4:04 PM
 */
?>

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Customers</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="link-color" href="#">Home</a></li>
                    <li class="breadcrumb-item active">customers</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
@stop


@section('content')
    <div class="card">

    <!-- /.card-header -->
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Address</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($users as $customer)
                <tr>
                    <td>
                        {{$customer->name}}

                    </td>
                    <td>
                        {{$customer->email}}
                    </td>
                    <td></td>

                    <td>

                    </td>

                    <td></td>
                    <td></td>

                </tr>
            @endforeach
        </table>
    </div>
    <!-- /.card-body -->
    </div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/public/css/dataTables.bootstrap4.css">
@stop

@section('js')
    <script src="/js/jquery.dataTables.js"></script>
    <script src="/js/dataTables.bootstrap4.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
@stop


