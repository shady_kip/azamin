<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/9/2020
 * Time: 3:16 PM
 */
?>
@extends('client.layout.app')
@section('title', 'Shopping made easy')
@section('content')
    <div class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        @foreach(\App\Category::ancestorsOf($product->category_id) as $crumb)
        <li class="breadcrumb-item"><a href="/products/category/{{$crumb->slug}}">
             {{$crumb->name}}
            </a></li>
            @foreach($crumb->children as $cr)
                @if($cr->id==$product->category_id)
                <li class="breadcrumb-item"><a href="/products/category/{{$cr->slug}}">
                        {{$cr->name}}
                    </a></li>
                @endif
                @endforeach
        @endforeach

        <li class="breadcrumb-item active" aria-current="page">{{$product->title}}</li>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-lg-6">
                    <br/>
                <div class="details-header">
                <h4>{{$product->title}}</h4>
                    <div class="details-extras">
                        <div class="brand">Brand:&nbsp;<a href="/products/category/audios-theaters?brand=
                        @if(isset($product->brand_id))
                    {{\App\Brand::find($product->brand_id)->name}}
                                    @endif

                                    ">
                                @if($product->brand_id!==null)
                                {{\App\Brand::find($product->brand_id)->name}}
                                    @else
                                -
                                    @endif
                            </a> </div>
                        <div class="review">
                            <span class="float-left">
                                   <i style="color: #edb600" class="material-icons ">star_outline</i>
                            <i style="color: #edb600" class="material-icons">star_outline</i>
                            <i style="color: #edb600" class="material-icons">star_outline</i>
                            <i style="color: #edb600" class="material-icons">star_outline</i>
                            </span>

                            (1 review)
                        </div>
                        <div class="sku">
                            SKU: {{$product->SKU}}
                        </div>
                    </div>
                </div>


                <ul id="lightSlider">

                    @foreach(\App\Gallery::where('product_id',$product->stock_id)->get() as $photo)
                    <li data-thumb="/product_images/resized/{{$photo->name}}">
                        <img style="max-height: 500px" src="/product_images/original/{{$photo->name}}" />
                    </li>
                        @endforeach



                </ul>

                <!-- Root element of PhotoSwipe. Must have class pswp. -->
                <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

                    <!-- Background of PhotoSwipe.
                         It's a separate element as animating opacity is faster than rgba(). -->
                    <div class="pswp__bg"></div>

                    <!-- Slides wrapper with overflow:hidden. -->
                    <div class="pswp__scroll-wrap">

                        <!-- Container that holds slides.
                            PhotoSwipe keeps only 3 of them in the DOM to save memory.
                            Don't modify these 3 pswp__item elements, data is added later on. -->
                        <div class="pswp__container">
                            <div class="pswp__item"></div>
                            <div class="pswp__item"></div>
                            <div class="pswp__item"></div>
                        </div>

                        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                        <div class="pswp__ui pswp__ui--hidden">

                            <div class="pswp__top-bar">

                                <!--  Controls are self-explanatory. Order can be changed. -->

                                <div class="pswp__counter"></div>

                                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                                <button class="pswp__button pswp__button--share" title="Share"></button>

                                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                                <!-- element will get class pswp__preloader--active when preloader is running -->
                                <div class="pswp__preloader">
                                    <div class="pswp__preloader__icn">
                                        <div class="pswp__preloader__cut">
                                            <div class="pswp__preloader__donut"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                <div class="pswp__share-tooltip"></div>
                            </div>

                            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                            </button>

                            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                            </button>

                            <div class="pswp__caption">
                                <div class="pswp__caption__center"></div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <div class="col-lg-6">
                <div style="border: none" class="card">
                    <div class="card-body">
                        <div style="display: flex;flex-direction: row;align-items: center;justify-content: flex-start" class="price">

                            <div>
                                @if($product->product_type=='variable-product')
                                    <b>Ksh     {{ collect(\App\Variation::where('product_id',$product->id)->get())->min('price')}}  -   {{ collect(\App\Variation::where('product_id',$product->id)->get())->max('price')}}</b>
                                @else
                                <h3 style="font-weight: 700">{{number_format($product->selling_price)}}&nbsp;<sup>KSH</sup></h3>
                                @endif
                            </div>

                            <div style="margin-left: 5px"> <h6 style="text-decoration: line-through">KSH {{number_format($product->cost_price)}}</h6></div>
                            <div style="margin-left: 5px;color: crimson">(-5% off)</div>


                        </div>
                        <div> Status: <span style="color: #00b44e">In Stock</span></div>

<hr>
                        @if($product->features !==null)
                        <h4>Features</h4>

                        {!! $product->features !!}
<hr/>
                        @endif
                        <div class="qty-cart">
                            <div>
                                <div class="qty-box">
                                    <div  class="icon_minus-06">
                                        <a  class="btn  mb-2  btn-sm"><i class="material-icons">minimize</i></a>

                                    </div>
                                    <div>
                                    <input type="number" id="quantity_5e674510c7979" class="input-text text-center qty text" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" inputmode="numeric">
                                    </div>
                                    <div class=" icon_plus">

                                        <a  class="btn  mb-1  btn-sm"> <i class="material-icons mt-2 ">add</i></a></div>
                                </div>

                            </div>
                            <div>
                                <button type="button" class="btn btn-warning">Add To Cart</button>
                            </div>
                            <div>
                                <i class="material-icons">
                                    favorite_border
                                </i>
                            </div>
                        </div>
                        <hr/>
                        <div>
                            Categories:
                            @foreach(\App\Category::ancestorsOf($product->category_id) as $cat)

                                <b>{{$cat->name}}&nbsp;,</b>
                                @endforeach

                        </div>
                        <div>
                            Tags: bag, clothing
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <br/>
        <br/>
        <br/>
            <div style="border: none" class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Description</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Reviews</a>
                        </li>

                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <br/>
                         <p>{!! $product->description !!}</p>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">reviews</div>
                    </div>
                </div>
            </div>
    </div>

@stop

@section('css')
    <style>
        .breadcrumb{
            height: auto;
        }
        .details-header{
            height: 100px;
            background-color: #f8f9fa;
            padding: 5px;

        }
        .details-header h4{
            text-align: center;
            font-family: Ubuntu;
        }
        .details-extras,.qty-cart{
            display: flex;
            flex-direction: row;
            justify-content: space-evenly;
        }
        .qty-box{

            display: flex;
            flex-direction: row;
            justify-content: flex-start !important;
            align-items: center !important;

            border:1px solid #e3e3e3;
            height: 30px;

        }

        .input-text{
            border:none;
            width: 40px;
        }
        .nav-tabs{
            justify-content: center;
        }
        .nav-item>a{
            border-top: none !important;
            border-left: none !important;
            border-right: none !important;
            text-transform: uppercase;
            color: var(--foreground-default);
            font-weight: 700;
            border-bottom: 1px solid darkgrey ;
        }
        .nav-item>a.active{
            border-bottom: 1px solid orange !important;
            color: orange !important;
        }
        .nav-tabs{
            border-bottom: none !important;
        }

    </style>
    <link rel="stylesheet" href="{{asset('/vendor/light-slider/css/lightslider.min.css')}}">

@stop

@section('js')
    <script src="{{asset('/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('/vendor/light-slider/js/lightslider.min.js')}}"></script>
    <script src="{{asset('/vendor/photoswipe/photoswipe.js')}}"></script>
    <script src="{{asset('/vendor/photoswipe/photoswipe-ui-default.js')}}"></script>
 <script>
     $("#lightSlider").lightSlider({
         gallery: true,
         item: 1,
         loop:true,
         slideMargin: 0,

         mode: "slide",
         useCSS: true,
         cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
         easing: 'linear'
     });


     var pswpElement = document.querySelectorAll('.pswp')[0];

     // build items array
     var items = [
         {
             src: 'https://placekitten.com/600/400',
             w: 600,
             h: 400
         },
         {
             src: 'https://placekitten.com/1200/900',
             w: 1200,
             h: 900
         }
     ];

     // define options (if needed)
     var options = {
         // optionName: 'option value'
         // for example:
         index: 0 // start at first slide
     };

     // Initializes and opens PhotoSwipe
     var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
   //  gallery.init();
 </script>
@stop

