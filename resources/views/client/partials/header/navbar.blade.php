<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/6/2020
 * Time: 11:45 AM
 */
?>

<div class="top-bar">
    <div class="container top-bar-nav">
        <div class="app-logo">
            <img  style="max-height: 50px" src="./../../assets/logo.png">
        </div>
        <div></div>
        <div class="top-bar-right">
            <div><i class="material-icons float-left">
                    place
                </i> Store location&nbsp;&nbsp;|&nbsp;&nbsp;</div>
            <div><i class="material-icons float-left">
                    local_shipping
                </i>Track your order&nbsp;&nbsp;|&nbsp;&nbsp;</div>
            <div><i class="material-icons float-left">
                    help
                </i>Help&nbsp;&nbsp;</div>
        </div>
    </div>
</div>
<div class="nav-primary hs-menubar">
    <div class="navs container">
        <div    class="show-cat menu-trigger "  style="cursor: pointer">
            <i class="material-icons menu-icon float-left">
                dehaze
            </i>&nbsp;
            <i style="display: none" class="material-icons float-left hidden-icon">
                clear
            </i>
            Categories

        </div>
        <div >
            <form class="navbar-search" method="get" action="" autocomplete="off">
                <label class="sr-only screen-reader-text" for="search">Search for:</label>
                <div class="input-group">
                    <div class="input-search-field">
                        <input type="text" id="search" class="form-control search-field product-search-field" dir="ltr" value="" name="s" placeholder="I am shopping for..." autocomplete="off"></div>
                    <div class="input-group-addon search-categories">
                        <select name="product_cat" id="electro_header_search_categories_dropdown" class="postform resizeselect" style="width: 143px;">
                            <option value="0" selected="selected">All Categories</option>
                            @foreach(\App\Category::with('ancestors')->get()->toTree() as $categories)
                                <optgroup label="{{$categories->name}}">
                                    @foreach($categories->children as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </optgroup>

                                @endforeach

                        </select></div>
                    <div class="input-group-btn">
                        <input type="hidden" id="search-param" name="post_type" value="product">
                        <button type="submit" class="btn has-tertiary-background btn-navbar-search btn-secondary">Search&nbsp;<i class="material-icons float-right">search</i></button>
                    </div>
                </div>
            </form>
        </div>
        <div>
            <div>
                <a href="/auth/login" class="has-foreground btn btn-default">Login/Signup</a>

            </div>

        </div>
    </div>
</div>

<div class="mobile-search-form">
    <div class="input-group mobile-search-input ">
        <input id="mob-search-form" type="text" class="form-control" placeholder="Search for products, categories" aria-label="Recipient's username" aria-describedby="basic-addon2">

    </div>
</div>

<nav class="hs-navigation">
    <ul class="nav-links">
        {{--<li><a href="#4"> <span class="icon"> <i class="zmdi zmdi-collection-item"></i> </span> List Item one</a> </li>--}}

        @foreach(\App\Category::with('ancestors')->get()->toTree() as $categories)
        <li class="has-child">
               <span class="its-parent">
               <span class="icon"> <i class="zmdi zmdi-device-hub"></i>
               </span>{{$categories->name}}</span>
            @foreach($categories->children as $item)
            <ul class="its-children">
                @if(count($item->children)>0)
                    <li class="has-child">
                        <span class="its-parent"> {{$item->name}}</span>
                        <ul class="its-children">
                            @foreach($item->children as $child)
                            <li> <a href="/products/category/{{$child->slug}}">{{$child->name}} </a> </li>
                                @endforeach

                        </ul>
                    </li>
                    @else
      <li><a href="/products/category/{{$item->slug}}"> {{$item->name}} </a> </li>
                    @endif



            </ul>
                @endforeach
        </li>
        @endforeach
        {{--<li> <a href="#4"> <span class="icon"> <i class="zmdi zmdi-compass"></i> </span> List three</a> </li>--}}
        {{--<li> <a href="#4"> <span class="icon"> <i class="zmdi zmdi-collection-video"></i> </span> List Item four</a> </li>--}}
        {{--<li class="has-child">--}}
               {{--<span class="its-parent">--}}
               {{--<span class="icon"> <i class="zmdi zmdi-wrap-text"></i>--}}
               {{--</span>Another Dropdown</span>--}}
            {{--<ul class="its-children">--}}
                {{--<li><a href="#1"> dropdown item 1  </a> </li>--}}
                {{--<li> <a href="#1"> dropdown item 2 </a> </li>--}}
                {{--<li> <a href="#1">  dropdown item 3 </a> </li>--}}
                {{--<li> <a href="#1"> dropdown item 4</a> </li>--}}
            {{--</ul>--}}
        {{--</li>--}}
        <!--//has-child-->
    </ul>
</nav>
<!--End hs Mega Menu-->

{{--<div id="sidenav" class="sidenav">--}}
    {{--<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>--}}
    {{--<div class="menu-items">--}}
        {{--<div class="menu-header text-center">--}}
            {{--<h5>Categories</h5>--}}
            {{--<hr/>--}}
        {{--</div>--}}
        {{--<div class="menu-item">Electronics</div>--}}
        {{--<div class="menu-item">Phones & Accessories</div>--}}
        {{--<div class="menu-item">Computing</div>--}}
        {{--<div class="menu-item">Furniture</div>--}}
        {{--<div class="menu-item">Home & Living</div>--}}
    {{--</div>--}}
{{--</div>--}}