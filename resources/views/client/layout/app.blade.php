<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/6/2020
 * Time: 11:39 AM
 */
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Azamin.co.ke | @yield('title')</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/5.0.1/collection/components/icon/icon.min.css">
    <link rel="stylesheet" href="{{asset('/css/theme.css')}}">
    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/navbar/nav.css')}}">
    <!--Material Design Iconic Font-->
    <link rel="stylesheet" href="/vendor/menu/material-design/css/material-design-iconic-font.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{asset('/vendor/photoswipe/photoswipe.css')}}">
    <link rel="stylesheet" href="{{asset('/vendor/photoswipe/default-skin/default-skin.css')}}">
    <link rel="stylesheet" href="{{asset('/css/footer/footer.css')}}">
    <link rel="stylesheet" href="/vendor/menu/css/hs-menu.css" />

    @yield('css')
</head>
<body>
@include('client.partials.header.navbar')
<main class="all-content">
@yield('content')
</main>
<br/>
<br/>
<br/>


@yield('js')

<script src="{{asset('/vendor/photoswipe/photoswipe.js')}}"></script>
<script src="{{asset('/vendor/photoswipe/photoswipe-ui-default.js')}}"></script>
<script src="/vendor/menu/js/jquery.hsmenu.min.js"></script>
<script src="{{asset('/js/app.js')}}"></script>

</body>
</html>
