<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/6/2020
 * Time: 11:34 AM
 */
?>
@extends('client.layout.app')
@section('title', 'Shopping made easy')
@section('content')

<div class="container-fluid">
    <br/>
    <br/>
    <div class="row">
        <div class="col-lg-2 left-side-bar">
            <h6>Categories</h6>

            <br/>

            <h6>By Brands</h6>
            <br/>
            @if(isset($brands))
            @foreach($brands as $brand)
                    <div class="form-group form-check">

                        <input name="{{$brand->name}}"


                               onclick="insertParam('{{request()->url()}}','brand','{{$brand->name}}')

                                " type="checkbox" @if(request()->get('brand')==$brand->name) checked @endif class="form-check-input" id="check{{$brand->id}}">
                        <label class="form-check-label" for="check{{$brand->id}}"> {{$brand->name}}</label>

                    </div>

            @endforeach
            @endif
            <h6>By Price</h6>


        </div>
        <div class="col-lg-10">

            <!-- As a link -->
            <nav class="navbar navbar-light bg-light">
                <a style="font-size: 15px" class="navbar-brand " href="#"><b>{{count($products)}}</b> Products</a>
                <div class="navbar-text filter">

                    <div style="margin-right: 10px">
                        <select >
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div style="margin-right: 10px">View</div>
                    <div style="margin-right: 10px">
                        <i class="material-icons">
                            view_headline
                        </i>
                    </div>

                    <div style="margin-right: 10px">
                        <i class="material-icons">
                            view_list
                        </i>
                    </div>
    </div>
            </nav>

            @if(isset($sub_cat))

            <div class="scrollmenu">

            @foreach(\App\Category::descendantsOf($sub_cat->id) as $cats )

                @if(count($cats->children)>0)


            <a href="/products/category/{{$cats->slug}}" class="btn mb-2 btn-sm btn-outline-warning">{{$cats->name}}</a>
                    @endif

            @endforeach
                <br/>
            </div>
            @endif


            <div class="row">

                @foreach($products as $product)
                <div class="col-lg-3 col-sm-6 mt-3">

                    <div onclick="window.location.href='/product/{{$product->slug}}'" class="product-card">
                        <div class="product-card-header">
                            <div class="product-category has-primary-text">
                                @if($product->category_id!==null)
                                    {{\App\Category::find($product->category_id)->name}}
                                @else
                                    -

                                @endif
                            </div>
                            <div class="product-title">{{ \Illuminate\Support\Str::limit($product->title, $limit = 150, $end = '...')}}</div>
                        </div>
                        <div class="product-card-image">
                            <img src="/product_images/resized/{{$product->featured_image}}">
                        </div>
                        <div class="product-card-description">
                            <div class="product-price">
                                <b>Ksh {{number_format($product->selling_price)}}</b></div>
                            <div class="product-add-to-cart"> <button class="btn btn-sm btn-warning" style="outline: none !important;" mat-mini-fab>
                                    <i class="material-icons">
                                        add_shopping_cart
                                    </i>
                                </button></div>
                        </div>
                        <br/>
                        <div class="product-card-extras">

                            <div title="Add to favortes" class="add-to-wishlist">
                                <i class="material-icons">
                                    favorite_border
                                </i>
                            </div>
                            <div title="Reviews" class="rating">
                                <i class="material-icons float-left">
                                    star_border
                                </i>4.5
                            </div>
                        </div>
                    </div>
                </div>
@endforeach




            </div>

        </div>
    </div>
</div>

@stop

@section('css')
    <link rel="stylesheet" href="{{asset('/css/home/home.css')}}">
<style>
    .left-side-bar{
        background-color: #f5f5f5;
        height: auto;
        padding: 15px;
    }
    .filter{
        display: flex;
        flex-direction: row;
        justify-content: flex-end;
        align-items: center;

    }
    [type=checkbox]:after {
        content: attr(value);

        display: inline-block;
        white-space:nowrap;
        cursor:pointer;

    }

    div.scrollmenu {
        overflow: auto;
        white-space: nowrap;

    }

    div.scrollmenu::-webkit-scrollbar{
        height:3px;
        padding: 2px;
    }


    .product-card{
        width: 100%;
        background-color: white;
        border-radius: 10px;
        padding: 10px;
        height: auto;
        cursor: pointer;

    }
    .product-card-image>img{
        max-width: 100%;
    }

    .product-title{
        color: var(--background-secondary);
    }
    .product-category{
        font-size: 13px;
    }
    .product-card-description,.product-card-extras{
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;

    }
    .product-card-extras{
        background-color:#fafafa ;
        display: none;
    }
</style>
@stop

@section('js')
    <script src="{{asset('/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('/js/bootstrap.min.js')}}"></script>
    <script>
        function insertParam(currentUrl,key, value)
        {



            var kvp = document.location.search.substr(1).split('&');
            if (kvp == '') {
                document.location.search = '?' + key + '=' + value;
            }else {

                var i = kvp.length; var x;
                while (i--) {
                    x = kvp[i].split('=');
                    if (x[0] == key && x[1] == value) {
                        x[1] = value;
                        kvp[i] = x.join('=');
                        if(kvp.includes(kvp[i])){
                            kvp.splice(kvp.indexOf(kvp[i]));
                        }
                        break;
                    }
                }
                if (i < 0) {
                    kvp[kvp.length] = [key, value].join('=');

                }
                if(kvp.length==0){
                    var uri = window.location.toString();
                    if (uri.indexOf("?") > 0) {
                        var clean_uri = location.protocol + "//" + location.host + location.pathname;
                        window.history.replaceState({}, document.title, clean_uri);
                        location.reload(true)
                    }
                }else{
                    document.location.search = kvp.join('&');
                }
            }
        }


    </script>
@stop
