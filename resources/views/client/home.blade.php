<?php
/**
 * Created by PhpStorm.
 * User: _ck_
 * Date: 3/6/2020
 * Time: 11:34 AM
 */
?>
@extends('client.layout.app')
@section('title', 'Shopping made easy')
@section('content')

    <div class="owl-carousel owl-carousel1 owl-theme">
        <div class="home-slide"> <img src="{{asset('/images/bg/1.jpg')}}"> </div>
        <div class="home-slide"> <img src="{{asset('/images/bg/2.jpg')}}"> </div>
        <div class="home-slide"> <img src="{{asset('/images/bg/3.jpg')}}"> </div>


    </div>
    <div class="container">
        <div style="border: 0px" class="card">
            <br/>
            <div class="home-header text-center">
                <h4>Why shop with us</h4>
            </div>

        <div class="row card-body">
            <div class="col-lg-4 col-flex">
                <div class="free-shipping">
                    <i class="material-icons icon-large float-left has-primary-text">
                        local_shipping
                    </i>
                </div>

                <div class="content-right">
                    <h5>Free Shipping</h5>
                    <p>Same Day Delivery on All Nairobi Orders</p>
                </div>

            </div>
            <div class="col-lg-4 col-flex">
                <div class="free-shipping">
                    <i class="material-icons icon-large float-left has-primary-text">
                        credit_card
                    </i>
                </div>

                <div class="content-right">
                    <h5>Secure Payment</h5>
                    <p>Pay with MPesa Online OR Upon Delivery</p>
                </div>

            </div>
            <div class="col-lg-4 col-flex">
                <div class="free-shipping">
                    <i class="material-icons icon-large float-left has-primary-text">
                        settings_backup_restore
                    </i>
                </div>

                <div class="content-right">
                    <h5>Easy Return</h5>
                    <p>If goods have problems</p>
                </div>

            </div>
        </div>
    </div>
    </div>

    <div class="container">
        <!--Ads banner-->
        <img style="max-width: 100%" src="{{asset('/images/ads/home-v5-banner.png')}}">
    </div>
    <!--deals of the day-->

    <div class="featured_deals">
        <div style="border:none !important;width: 82%;margin: 0 auto" class="card container">
            <div class="header card-body text-center">
                <h4>Top Deals</h4>
            </div>
        </div>
        <br/>


        <div class="container">
        <div class="owl-carousel owl-carousel2 owl-theme">


            @foreach(\App\Product::orderBy('id','DESC')->take(10)->get() as $product)

            <div onclick="window.location.href='/product/{{$product->slug}}'" class="product-card">
                <div class="product-card-header">
                    <div class="product-category has-primary-text">
                        @if($product->category_id!==null)
                            {{\App\Category::find($product->category_id)->name}}
                        @else
                            -

                        @endif
                    </div>
                    <div class="product-title">{{ \Illuminate\Support\Str::limit($product->title, $limit = 150, $end = '...')}}</div>
                </div>
                <div class="product-card-image">
                    <img src="/product_images/resized/{{$product->featured_image}}">
                </div>
                <br/>
                <div class="product-card-description">
                    <div class="product-price">

                        @if($product->product_type=='variable-product')
                            <b>Ksh     {{ collect(\App\Variation::where('product_id',$product->id)->get())->min('price')}}  -   {{ collect(\App\Variation::where('product_id',$product->id)->get())->max('price')}}</b>
                            @else
                        <b>Ksh {{number_format($product->selling_price)}}</b>
                        @endif
                    </div>
                    <div class="product-add-to-cart"> <button style="outline: none !important;" class="btn btn-sm btn-warning" mat-mini-fab>
                            <i class="material-icons">
                                add_shopping_cart
                            </i>
                        </button></div>
                </div>
                <br/>
                <div class="product-card-extras">

                    <div title="Add to favortes" class="add-to-wishlist">
                        <i class="material-icons">
                            favorite_border
                        </i>
                    </div>
                    <div title="Reviews" class="rating">
                        <i class="material-icons float-left">
                            star_border
                        </i>4.5
                    </div>
                </div>
            </div>

@endforeach


        </div>

        </div>
    </div>


    <!--info banner-->
    {{--<div style="border: none !important;" class="container card">--}}
        {{--<div class="card-body">--}}
            {{--<div class="row ">--}}
                {{--<div class="col-lg-6">--}}
                    {{--<div class="section-description">--}}
                        {{--<div></div>--}}
                        {{--<div class="section-content text-center">--}}

                            {{--<h4>--}}
                                {{--Azamin makes it easy to buy--}}
                                {{--from products from  all over Kenya.--}}
                            {{--</h4>--}}
                            {{--<p>--}}
                                {{--We are much more than just a market--}}
                            {{--</p>--}}
                            {{--<br/>--}}
                            {{--<button class="has-tertiary-background btn btn-lg">Explore Products</button>--}}
                            {{--<br/>--}}
                            {{--<br/>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-6 section-image">--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}


    <div class="featured_categories">
        <div style="border: none !important;" class="card container">
            <div class="card-body">
                <div class="header text-center">
                    <h4>Shop By <span class="has-primary-text">Category</span></h4>
                </div>
                <br/>
                <div class="row">
                    @foreach(\App\Category::with('ancestors')->get()->toTree() as $category)
                    <div class="col-lg-3 mt-2  text-center">
                        <div onclick="navNext('{{$category->slug}}')"  class="card" style="width: 100%;border-radius:0px !important;cursor: pointer">

                            <img class="card-img-top"   src="{{asset('/category_images/'.$category->image)}}"/>
                            <div style="padding: 5px" class="card-body">
                                <h5 class="card-title">  {{$category->name}}</h5>
                                @foreach($category->children->take(3) as $item)

                                   <small>{{$item->name}},</small>
                                    @endforeach
                            </div>
                        </div>

                    </div>
                        @endforeach

                </div>
                <br/>
                <div class="text-center">
                    <a href="/products" class="btn theme-btn btn-warning btn-sm">Shop Now&nbsp;<i class="fa fa-arrow-right"></i> </a>
                </div>

            </div>
        </div>
    </div>
    <br/>
    <br/>
@stop

@section('css')
<link rel="stylesheet" href="{{asset('/css/home/home.css')}}">
@stop

@section('js')
    <script src="{{asset('/vendor/jquery/jquery.js')}}"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="{{asset('/js/home.js')}}"></script>
@stop
