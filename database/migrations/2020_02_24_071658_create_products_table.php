<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->longText('features')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('sub_categories_id')->nullable();
            $table->string('SKU')->nullable();
            $table->integer('cost_price')->nullable();
            $table->integer('selling_price')->nullable();
            $table->longText('variations')->nullable();
            $table->boolean('featured')->default(0);
            $table->string('featured_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
